﻿Param(
  [string]$sourcedir,
  [string]$sqlplusconnectstring,
  [string]$schema,
  [string]$version,
  [string]$sqlobjectdeployer_OBJECT_TABLE
)
$ErrorActionPreference = "Stop"

Write-Host ""
Write-Host "Start of sqlcodedeploy powershell script"

$maintmpdir = [System.IO.Path]::GetTempPath()

#delete any tmp dirs more than 3 days old
$limit = (Get-Date).AddDays(-3)
Get-ChildItem $maintmpdir | ?{ $_.PSIsContainer -and $_.FullName -like '*sqlcodedeploy_tmpscript_*' -and $_.CreationTime -lt $limit} | Remove-Item -Force -Recurse

$num = (Get-Random)
$thissqldir = $maintmpdir + "sqlcodedeploy_tmpscript_" + $num
mkdir $thissqldir
Write-Host "Tmp directory for sql $thissqldir"

$outfile = $maintmpdir + "sqlobjectdeveloper_getobjs_" + $num + ".out"
#Write-Host $outfile

$strFil=$env:CIUTILS_HOME + "/sqlcodedeploy/sqlobjectdeployer_getobjs.sql"
if (!(Test-Path $strFil)) {
    Write-Host "Error couldn't find sql $strFil"
    exit 1
}
sqlplus $sqlplusconnectstring "@$strFil" "$schema" "$sqlobjectdeployer_OBJECT_TABLE" "$outfile"
if ( ! ( $? )) {
	echo "sqlobjectdeployer_getobjs: Error in SQL - couldn't get objects (sqlobjectdeveloper_getobjs.sql)"
	exit 1
}

##Compile java if required
if (-not (Test-Path $env:CIUTILS_HOME/sqlcodedeploy/java/Main.class)) { 
    cd $env:CIUTILS_HOME/sqlcodedeploy/java
    javac -cp $env:CIUTILS_HOME/sqlcodedeploy/java/commons-codec-1.10.jar  Main.java
    if ( ! ( $? )) {
	    echo "ERROR Failed to compile"
	    exit 1
    }
    if (-not (Test-Path $env:CIUTILS_HOME/sqlcodedeploy/java/Main.class)) { 
        echo "ERROR Failed to find .class file"
        exit 1
    }
}

##Call java with $sourcedir $outfile $thissqldir $sqlobjectdeployer_OBJECT_TABLE $schema $version
##Java will output SQL files to run in $thissqldir
java -cp $env:CIUTILS_HOME/sqlcodedeploy/java/commons-codec-1.10.jar`;$env:CIUTILS_HOME/sqlcodedeploy/java Main "$sourcedir" "$outfile" "$thissqldir" "$sqlobjectdeployer_OBJECT_TABLE" "$schema" "$version"
##Write-Host java -cp $env:CIUTILS_HOME/sqlcodedeploy/java Main "$sourcedir" "$outfile" "$thissqldir" "$sqlobjectdeployer_OBJECT_TABLE" "$schema" "$version"
if ( ! ( $? )) {
	exit 1
}


del $outfile

##Go trhough $thissqldir find highest numbered file 00001.sql, 00002.sql, etc (5 digits)


Write-Host ""
Write-Host "-----------------------------------------------------"
Write-Host "Starting sqlcodedeploy powershell script run"
Write-Host "-----------------------------------------------------"
$curfil=1
while (Test-Path $thissqldir\$($curfil.ToString("00000")).sql) {
    $strFil = $thissqldir + "\" + $($curfil.ToString("00000")) + ".sql"
    Write-Host $strFil
    sqlplus $sqlplusconnectstring "@$strFil" "$schema" "$sqlobjectdeployer_OBJECT_TABLE" "$outfile"
    if ( ! ( $? )) {
	    echo "sqlobjectdeployer: Error in SQL"
	    exit 1
    }

    $curfil += 1
}
Write-Host "-----------------------------------------------------"
$curfil -= 1
Write-Host "End sqlcodedeploy powershell run ($curfil files executed)"
Write-Host "-----------------------------------------------------"

Write-Host "End of sqlcodedeploy powershell script"
Write-Host ""

exit 0