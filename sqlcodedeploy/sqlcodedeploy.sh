#!/bin/bash

echo ""
echo "sqlcodedeploy.sh - Script to deploy database code to database"

#The following line makes the script exit whenever an error occurs
set -e

CODEDIR=${1}
SQLPLUSCONNECTSTRING=${2}
SCHEMA=${3}
VERSION=${4}
sqlobjectdeployer_OBJECT_TABLE=${5}

#Delete tmp scripts older than 3 days - this will account for failures
for dir_to_del in $(ls -d /tmp/*/ | grep '^/tmp/sqlcodedeploy_tmpscript_'); do
	#echo "TODEL:" ${dir_to_del}
	#ISOLDER=$(find ${dir_to_del}) #for testing will delete all
	ISOLDER=$(find ${dir_to_del} -ctime +3)
	if [ ${#ISOLDER} -gt 1 ]; then
		#Safety feature used before we do a rm -rf
		if [ "/tmp/" == ${ISOLDER:0:5} ]; then
			rm -rf ${ISOLDER}
		fi
	fi
done

#Get a name for a tmp script
SQLTMPSCRIPTDIR=/tmp/sqlcodedeploy_tmpscript_${RANDOM}
while [ -f ${SQLTMPSCRIPTDIR} ]; do
	SQLTMPSCRIPTDIR=/tmp/sqlcodedeploy_tmpscript_${RANDOM}
done
mkdir -p ${SQLTMPSCRIPTDIR}
echo "Tmp directory for sql ${SQLTMPSCRIPTDIR}"

#Get an acceptable name for an outfile
OUTFILE="/tmp/sqlobjectdeveloper_getobjs_${RANDOM}.out"
while [ -f ${OUTFILE} ]; do
	OUTFILE="/tmp/sqlobjectdeveloper_getobjs_${RANDOM}.out"
done

sqlplus ${SQLPLUSCONNECTSTRING} "@${CIUTILS_HOME}/sqlcodedeploy/sqlobjectdeployer_getobjs.sql" "${SCHEMA}" "${sqlobjectdeployer_OBJECT_TABLE}" "${OUTFILE}"
SQLPLUS_RESULT=${?}
if [ ${SQLPLUS_RESULT} -ne 0 ]; then
	echo "sqlobjectdeployer_getobjs: Error in SQL - couldn't get objects (sqlobjectdeveloper_getobjs.sql)"
	exit 1
fi

##Compile java if required
if [ ! -f ${CIUTILS_HOME}/sqlcodedeploy/java/Main.class ]; then
	cd ${CIUTILS_HOME}/sqlcodedeploy/java
    javac -cp ${CIUTILS_HOME}/sqlcodedeploy/java/commons-codec-1.10.jar Main.java
	if [ ! -f ${CIUTILS_HOME}/sqlcodedeploy/java/Main.class ]; then
        echo "ERROR Failed to find .class file"
        exit 1
	fi
fi

##Call java 
##Java will output SQL files to run in ${SQLTMPSCRIPTDIR}
java -cp ${CIUTILS_HOME}/sqlcodedeploy/java/commons-codec-1.10.jar:${CIUTILS_HOME}/sqlcodedeploy/java Main "${CODEDIR}" "${OUTFILE}" "${SQLTMPSCRIPTDIR}" "${sqlobjectdeployer_OBJECT_TABLE}" "${SCHEMA}" "${VERSION}"


rm ${OUTFILE}

SQLCURFILENUM=1
SQLCURFILE="00000${SQLCURFILENUM}"
SQLCURFILE=${SQLCURFILE:${#SQLCURFILE}-5}.sql

echo "-----------------------------------------------------"
echo "Starting SQL script run"
echo "-----------------------------------------------------"

while [ -f ${SQLTMPSCRIPTDIR}/${SQLCURFILE} ]
do
	echo "Executing ${SQLTMPSCRIPTDIR}/${SQLCURFILE}"
	
	sqlplus ${SQLPLUSCONNECTSTRING} "@${SQLTMPSCRIPTDIR}/${SQLCURFILE}"
	SQLPLUS_RESULT=${?}
	if [ ${SQLPLUS_RESULT} -ne 0 ]; then
		echo "Error in SQL - dump file was ${SQLTMPSCRIPTDIR}/${SQLCURFILE}"
		exit 1
	fi
	
	(( SQLCURFILENUM += 1 ))
	SQLCURFILE="00000${SQLCURFILENUM}"
	SQLCURFILE=${SQLCURFILE:${#SQLCURFILE}-5}.sql
done
(( SQLCURFILENUM-- ))
echo "-----------------------------------------------------"
echo "End SQL script run (${SQLCURFILENUM} files executed)"
echo "-----------------------------------------------------"

echo ""
echo "end of sqlcodedeploy.sh"
echo ""
exit 0

