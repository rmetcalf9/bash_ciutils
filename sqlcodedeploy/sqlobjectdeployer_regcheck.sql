WHENEVER SQLERROR EXIT SQL.SQLCODE;
SET SERVEROUTPUT on;
ALTER SESSION SET CURRENT_SCHEMA=&1;

--This script should check the object table and error if we are regressing to
-- a previous verision
declare
  p_version_we_are_installing varchar2(255) := '&3';
  
  cursor c_obj_versions is
    select so."version" as v
    from "&1"."schema_objects" so
    group by so."version"
  ;  
  l_obj_versions c_obj_versions%rowtype;
  
  procedure splitVersion(
    p_ver in varchar2,
    x_major out number,
    x_minor out number,
    x_build out number,
    x_snap out boolean
  )
  is
    l_tmp varchar2(255);
  begin
    l_tmp := upper(trim(p_ver));
    x_snap := false;
    if '-SNAPSHOT'=nvl(substr(l_tmp,-9),'N') then
      x_snap := true;
      l_tmp := substr(l_tmp,0,length(l_tmp)-9);
    end if;
  
    x_major := to_number(substr(l_tmp,0,instr(l_tmp,'.',1,1)-1));
    l_tmp := trim(substr(l_tmp,instr(l_tmp,'.',1,1)+1));
    --dbms_output.put_line(l_tmp);
    
    x_minor := to_number(substr(l_tmp,0,instr(l_tmp,'.',1,1)-1));
    l_tmp := trim(substr(l_tmp,instr(l_tmp,'.',1,1)+1));
    --dbms_output.put_line(l_tmp);

    x_build := to_number(l_tmp);

    --if x_snap then
    --  dbms_output.put_line(p_ver || '->' || to_char(x_major) || '_' || to_char(x_minor) || '_' || to_char(x_build) || '= SNAPSHOT');
    --else
    --  dbms_output.put_line(p_ver || '->' || to_char(x_major) || '_' || to_char(x_minor) || '_' || to_char(x_build) || '= NORM');
    --end if;
  end;


  function isGreater(
    p_ver1 in varchar2,
    p_ver2 in varchar2
  ) return boolean
  is
    l_major1 number;
    l_major2 number;
    l_minor1 number;
    l_minor2 number;
    l_build1 number;
    l_build2 number;
    l_snap1 boolean;
    l_snap2 boolean;
  begin
    splitVersion(
        p_ver => p_ver1,
        x_major => l_major1,
        x_minor => l_minor1,
        x_build => l_build1,
        x_snap => l_snap1
    );
    splitVersion(
        p_ver => p_ver2,
        x_major => l_major2,
        x_minor => l_minor2,
        x_build => l_build2,
        x_snap => l_snap2
    );
    if l_major1 > l_major2 then
      return true;
    end if;
    if l_major1 < l_major2 then
      return false;
    end if;
    if l_minor1 > l_minor2 then
      return true;
    end if;
    if l_minor1 < l_minor2 then
      return false;
    end if;
    if l_build1 > l_build2 then
      return true;
    end if;
    if l_build1 < l_build2 then
      return false;
    end if;
    if l_snap1 then
      --1 is a snapshot. It can never be greater than 2
      return false;
    else
      if l_snap2 then
        --1 not snapshot but 2 is
        return true;
      else
        --neiher snapshot
        return false;
      end if;
    end if;
    return false;
  end;
begin
  for l_obj_versions in c_obj_versions loop
    if true=isGreater(l_obj_versions.v,p_version_we_are_installing) then
      dbms_output.put_line('---------------------------------------------------');
      dbms_output.put_line('Installing Shema &1');
      dbms_output.put_line('Version ' || p_version_we_are_installing);
      dbms_output.put_line('Detected objects in DB with version ' || l_obj_versions.v);
      dbms_output.put_line('ERROR - Can''t install a previous version of schema');
      dbms_output.put_line('---------------------------------------------------');
      raise_application_error( -20001, 'ERROR - Can''t install a previous version of schema' );
    end if;
  end loop;

end;

/
exit