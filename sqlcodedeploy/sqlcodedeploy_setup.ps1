﻿Param(
  [string]$sourcedir,
  [string]$sqlplusconnectstring,
  [string]$schema,
  [string]$version,
  [string]$sqlobjectdeployer_OBJECT_TABLE
)
$ErrorActionPreference = "Stop"

Write-Host ""
Write-Host "Start of sqlcodedeploy_setup powershell script"
Write-Host "SourceDir=$sourcedir"
#Write-Host "sqlplusconnectstring=$sqlplusconnectstring" Removed so logs don't contain password
Write-Host "schema=$schema"
Write-Host "version=$version"

$strFil=$env:CIUTILS_HOME + "/sqlcodedeploy/sqlobjectdeployer_createtable.sql"
if (!(Test-Path $strFil)) {
    Write-Host "Error couldn't find sql $strFil"
    exit 1
}
sqlplus $sqlplusconnectstring as sysdba "@$strFil" "$schema" "$sqlobjectdeployer_OBJECT_TABLE"
if ( ! ( $? )) {
	echo "sqlobjectdeployer_setup: Error in SQL - couldn't setup schema (sqlobjectdeployer_createtable.sql)"
	exit 1
}


$strFil=$env:CIUTILS_HOME + "/sqlcodedeploy/sqlobjectdeployer_regcheck.sql"
if (!(Test-Path $strFil)) {
    Write-Host "Error couldn't find sql $strFil"
    exit 1
}
sqlplus $sqlplusconnectstring "@$strFil" "$schema" "$sqlobjectdeployer_OBJECT_TABLE" "$version"
if ( ! ( $? )) {
	echo "sqlobjectdeployer_setup: Error Can't install this version on this database"
	exit 1
}


Write-Host "End of sqlcodedeploy_setup powershell script"
Write-Host ""

exit 0