WHENEVER SQLERROR EXIT SQL.SQLCODE;
SET SERVEROUTPUT on;
ALTER SESSION SET CURRENT_SCHEMA=&1;

declare
	l_num number;
begin
	select count('x')
	into l_num
	from all_tables
	where table_name='&2'
	and owner='&1'
	;
	if l_num=0 then
		dbms_output.put_line('');
		dbms_output.put_line('Creating schema table');
		execute immediate 'create table "&2"
		(
		  "hash" VARCHAR2(1024) NOT NULL 
		, "file_name" VARCHAR2(1024) NOT NULL 
		, "version" VARCHAR2(255) NOT NULL 
		, "deployment_order" VARCHAR2(10) NOT NULL 
		, created_by VARCHAR2(30 CHAR) NOT NULL 
		, creation_date TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL 
		, last_updated_by VARCHAR2(30 CHAR) NOT NULL 
		, last_update_date TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL 
		, CONSTRAINT "' || '&2' || '_U01" UNIQUE ("hash")
		, CONSTRAINT "' || '&2' || '_U02" UNIQUE ("file_name")
		)'; 		
		
		execute immediate 'grant all on "&2" to CIDBADMIN';
		
		execute immediate 'CREATE OR REPLACE TRIGGER "' || '&2' || '_IU" 
BEFORE INSERT OR UPDATE
ON "&1"."&2"
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
  IF INSERTING THEN
    :new.created_by := user;
    :new.creation_date := SYSTIMESTAMP;
	
  END IF;
  IF INSERTING OR UPDATING THEN
    -- populate audit columns
    :new.last_updated_by := user;
    :new.last_update_date := SYSTIMESTAMP;
  END IF;
END ' || '&2' || '_IU;
	';
		
		
	end if;
	
	--Install check Error Procedure
	select count('x')
	into l_num
	from all_objects ao
	where ao.owner='&1'
	and object_type='PROCEDURE'
	and object_name='SCHEMA_DISPLAY_ERRORS'
	;
	if l_num=0 then
		dbms_output.put_line('');
		dbms_output.put_line('Creating error reporting procedure (SCHEMA_DISPLAY_ERRORS)');
		execute immediate 'CREATE OR REPLACE PROCEDURE SCHEMA_DISPLAY_ERRORS 
(
  P_SCHEMA IN VARCHAR2,
  P_OBJECT IN VARCHAR2,
  P_TYPE IN VARCHAR2 DEFAULT null
) AS 
  l_num_errors number;
  cursor c_errors is
    select ae.attribute || '':'' || ae.text as text2, ae.type,ae.line,ae.position
    from all_errors ae
    where ae.owner=''&1''
	and name = P_OBJECT
	and type = nvl(P_TYPE,type)
    order by ae.sequence
  ;
  l_errors c_errors%rowtype;
BEGIN
  select count(*)
    into l_num_errors
    from all_errors
    where owner = P_SCHEMA
	and name = P_OBJECT
	and type = nvl(P_TYPE,type)
   ;

 if( l_num_errors > 0 )
 then
  dbms_output.put_line('''');
  dbms_output.put_line(to_char(l_num_errors) || '' Errors in '' || P_SCHEMA || ''.'' || P_OBJECT);
  dbms_output.put_line(''------------------------------------------------'');
  for l_errors in c_errors loop
    dbms_output.put_line(''Type:'' || l_errors.type);
    dbms_output.put_line(''Line(Pos):'' || l_errors.line || ''('' || l_errors.position || '')'');
    dbms_output.put_line(l_errors.text2);
    dbms_output.put_line(''------------------------------------------------'');
  end loop;

  
  dbms_output.put_line('''');
  dbms_output.put_line('''');
    raise_application_error( -20001, to_char(l_num_errors) || '' Errors in '' || P_SCHEMA || ''.'' || P_OBJECT );
 end if;
END SCHEMA_DISPLAY_ERRORS;'; 		
	end if;
end;
/
exit
