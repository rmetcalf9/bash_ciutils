#!/bin/bash

#Setup of SQLCode deploy
# this script will create the object table if it dosen't exist
# the code will also check the contents of the object table and make sure that
# we are not regressing any objects to a previous version. If we are
# the script will error.
#
# This was seperated from the main sqlcodedeploy script so that we 
# can fail before we deploy any code at all to prevent partial deployments

SQLPLUSCONNECTSTRING=${2}
SCHEMA=${3}
VERSION=${4}
sqlobjectdeployer_OBJECT_TABLE=${5}

#If required then setup table
FIL=${CIUTILS_HOME}/sqlcodedeploy/sqlobjectdeployer_createtable.sql
if [ ! -f ${FIL} ]; then
	echo "Error couldn't find sql ${FIL}"
	exit 1
fi

#Have to connect as sysdba.
#I researched doing this without a sysdba connection:
#  http://stackoverflow.com/questions/39185899/table-permissions-with-table-created-with-an-altered-schema/39234740#39234740
#
sqlplus ${SQLPLUSCONNECTSTRING} as sysdba "@${FIL}" "${SCHEMA}" "${sqlobjectdeployer_OBJECT_TABLE}"
SQLPLUS_RESULT=${?}
if [ ${SQLPLUS_RESULT} -ne 0 ]; then
	echo "sqlobjectdeployer_setup: Error in SQL - couldn't setup schema (sqlobjectdeployer_createtable.sql)"
	exit 1
fi

FIL=${CIUTILS_HOME}/sqlcodedeploy/sqlobjectdeployer_regcheck.sql
echo " Checking there is no later version of schema ${SCHEMA} installed (Currently installing ${VERSION})..."
sqlplus ${SQLPLUSCONNECTSTRING} "@${FIL}" "${SCHEMA}" "${sqlobjectdeployer_OBJECT_TABLE}" "${VERSION}"
SQLPLUS_RESULT=${?}
if [ ${SQLPLUS_RESULT} -ne 0 ]; then
	echo "sqlobjectdeployer_setup: Error Can't install this version on this database"
	exit 1
fi



echo ""
echo "end of sqlcodedeploy_setup.sh"
echo ""

exit 0

