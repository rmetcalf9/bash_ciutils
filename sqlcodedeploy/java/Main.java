import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collections;
import java.io.PrintWriter;
import java.io.FileInputStream;

import org.apache.commons.codec.digest.DigestUtils;

public class Main {
	private static enum SQLFileType {
		PACKAGEHEAD("/Packages","_PKS.sql","010","drop package","'PACKAGE\'"),
		PACKAGEBODY("/Packages","_PKB.sql","060","drop package body","NULL"),
		FUNCTION("/Functions",".sql","020","drop function","NULL"),
		PROCEDURE("/Procedures",".sql","030","drop procedure","NULL"),
		TRIGGER("/Triggers",".sql","040","drop trigger","NULL"),
		VIEW("/Views",".sql","050","drop view","NULL")
		;
		
		private String m_directory;
		private String m_fileNameTerminator;
		private String m_SortKey;
		private String m_dropString;
		private String m_typeForErrorCheck;
		private SQLFileType(String p_directory, String p_fileNameTerminator, String p_SortKey, String p_dropString, String p_typeForErrorCheck) {
			m_directory = p_directory;
			m_fileNameTerminator = p_fileNameTerminator;
			m_SortKey = p_SortKey;
			m_dropString = p_dropString;
			m_typeForErrorCheck = p_typeForErrorCheck;
		};
		public String getTypeForErrorCheck() {
			return m_typeForErrorCheck;
		};
		public String getDirectory(String p_srcDir) {
			return p_srcDir + m_directory;
		};
		public String getRelativeDirectory() {
			return m_directory;
		};
		public String getObjectName(String p_filename) throws Exception {
			if ( p_filename.startsWith("/") ) throw new Exception("Invalid filename passed to getObjectName - " + p_filename + " (expecting filename without path)");
			return p_filename.substring(0,p_filename.length()-m_fileNameTerminator.length());
		};
		public boolean isSourceFileForThisType(String p_filename) {
			return p_filename.endsWith(m_fileNameTerminator);
		};
		public String getSortKey() {
			return m_SortKey;
		};
		public String getFileNameTerminator() {
			return m_fileNameTerminator;
		};
		
		public static SQLFileType getTypeFromSourcefile(String p_filename) {
			for (SQLFileType ft : SQLFileType.values()) {
				if (ft.isSourceFileForThisType(p_filename)) {
					if (p_filename.startsWith(ft.getRelativeDirectory())) {
						return ft;
					};
				}
			}			
			return null;
		};
		
		public String getDropString(String p_objectName) {
			return m_dropString + " " + p_objectName + ";";
		};
	}
	
	private static class SQLFile implements Comparable<SQLFile> {
		private String  m_FileName = "";
		private SQLFileType m_Type = null;
		private String m_SortKey = "";
		private String m_md5_hash = "";

		public SQLFile(String p_FileName, SQLFileType p_Type, String p_code_dir) throws Exception {
			m_FileName = p_FileName;
			m_Type = p_Type;
			
			//Read the sory key from the file if there is any
			m_SortKey = getSortKey(p_code_dir);
			
			FileInputStream fis = new FileInputStream(new File(p_code_dir + getRelativeFileName()));
			m_md5_hash = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis).trim();
			fis.close();
		};
		
		/*
		Used in constructor to read the file and extract a sort key
		# reads through the source file searching for the SQLCODEDEPLOYORDER tag. If not present default to 50000
		# numbers are up to a maximum of 5 digits and can't contain non-numeric characters
		# Example: --SQLCODEDEPLOYORDER:101
		# numbers must be positive		
		*/
		private String getSortKey(String p_code_dir) throws Exception {
			String return_value = "50000";
			File f = new File(p_code_dir + getRelativeFileName());
			if (!f.exists()) throw new Exception("Cannot find file " + p_code_dir + getRelativeFileName());
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			String lineWorking;
			boolean found = false;
			while ((line = br.readLine()) != null) {
				lineWorking = line.trim();
				if (lineWorking.startsWith("--SQLCODEDEPLOYORDER:")) {
					if (found) throw new Exception("ERROR " + p_code_dir + getRelativeFileName() + " contains mutiple \"--SQLCODEDEPLOYORDER:\" tags");
					found = true;
					if (lineWorking.length()>21) {
						lineWorking = lineWorking.substring(21);
						int n;
						try {
							n = Integer.parseInt(lineWorking);
						} catch (Exception ee) {
							throw new Exception("Invalid SQLCODEDEPLOYORDER - must be a number " + line);
						};
						if (n<0) throw new Exception("Invalid SQLCODEDEPLOYORDER - not be less than 0 " + line);
						if (n>99999) throw new Exception("Invalid SQLCODEDEPLOYORDER - not be greater than 99999 " + line);
						return_value = "00000" + Integer.toString(n).trim();
						return_value = return_value.substring(return_value.length()-5);
					};
				};
			}
			
			return return_value;
		};
		
		public String getObjectName() throws Exception {
			return m_Type.getObjectName(m_FileName);
		};
		public String getRelativeFileName() {
			return m_Type.getRelativeDirectory() + "/" + m_FileName;
		};
		public String getSortKey() {
			return m_SortKey + "_" + m_Type.getSortKey();
		};
		
		public String toStringX() throws Exception {
				return getObjectName() + "(" + getRelativeFileName() + ") has sort key " + getSortKey();
		};
		
		@Override
		public int compareTo(SQLFile p_o) {
			return getSortKey().compareTo(p_o.getSortKey());
		}		
		
		public SQLFileType getType() {return m_Type;};
		public String getMD5Hash() {return m_md5_hash;};

		public boolean hasChanged(Map<String,String> p_fileHashes) {
			String dbhash = p_fileHashes.get(getRelativeFileName());
			if (dbhash==null) return true;
			return (!dbhash.trim().equals(getMD5Hash()));
		};
	};
	
	/*
	class used to write output to sql files
	automatically goes over to new file based on bytes read
	e.g. 00001, 00002, etc.
	*/
	private static class SQLFileWriter {
		private int m_fileNum = 0;
		private final int m_maxChars = 4096; //actual chars will be a bit more than this
		private int m_curChars = 0;
		private String m_SQLOutputDir;
		private PrintWriter m_writer = null;
		private String m_schema = "";
		
		public SQLFileWriter(String p_SQLOutputDir, String p_schema) throws Exception {
			m_SQLOutputDir = p_SQLOutputDir;
			m_schema = p_schema;
			
			switchFile();
		};
		
		private void WriteStr(String p_data) throws Exception {
			m_curChars += p_data.length();
			m_writer.println(p_data);
		};
		
		public void Write(String p_sql) throws Exception {
			if (null==m_writer) {
				String newFileName = "00000" + Integer.toString(m_fileNum).trim();
				newFileName = m_SQLOutputDir + "/" + newFileName.substring(newFileName.length()-5) + ".sql";
				m_writer = new PrintWriter(newFileName, "UTF-8");
				
				WriteStr("WHENEVER SQLERROR EXIT SQL.SQLCODE;");
				WriteStr("ALTER SESSION SET CURRENT_SCHEMA=" + m_schema + ";");
				WriteStr("");

				//#Not putting serveroutput on all the time to prevent big install files causing issues
				//##will toggle on and off as required
			};
			WriteStr(p_sql);
			
			if (m_curChars>m_maxChars) switchFile();
		};
		
		private void switchFile() throws Exception {
			close();
			m_curChars = 0;
			m_fileNum++;
			//File not created until first write
		};
		
		public void close() throws Exception {
			if (null!=m_writer) {
				WriteStr("");
				WriteStr("COMMIT;");
				WriteStr("EXIT;");
				
				m_writer.close();
				m_writer = null;
			};
		};
	}
	
	private static void CheckAllPackageBodiesAndHeadersMatch(String sourcedir) throws Exception {
		File f = new File(SQLFileType.PACKAGEHEAD.getDirectory(sourcedir));
		if (f.exists()) { //Directory may not exist when we are deploying from resource jar
			Map<String,String> packageFiles = new HashMap<String,String>();
			File[] listOfFiles = f.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					if (listOfFiles[i].getName().endsWith(".sql")) {
						packageFiles.put(listOfFiles[i].getName(),listOfFiles[i].getName());
					};
				};
			};
			for (String key : packageFiles.keySet()) {
				if (key.endsWith("_PKB.sql")) {
					//Found a body, ensure we have a head
					if (null==packageFiles.get(key.substring(0,key.length()-8) + "_PKS.sql")) throw new Exception("No matching package header found for " + key);
				} else if (key.endsWith("_PKS.sql")) {
					//Found a head, ensure we have a body
					if (null==packageFiles.get(key.substring(0,key.length()-8) + "_PKB.sql")) throw new Exception("No matching package body found for " + key);
				} else {
					throw new Exception("Unrecognised package file - dosen't end with _PKB.sql or _PKS.sql");
				}
			};
		};
	};
	
	private static Map<String,String> ReadDBHashesSQLPlusOutputFile(String sqldeveloperGetObjsOutputFile) throws Exception {
		//Example line being output "OUTPUTLINE:41f9ae18d7d4aa386f067b54a1b47eb9:/Packages/SAMPLE_003_PKG_PKS.sql"
		Map<String,String> return_value = new HashMap<String,String>();
		File f = new File(sqldeveloperGetObjsOutputFile);
		if (!f.exists()) throw new Exception("SQLDeveloper output file dosen't exist - " + sqldeveloperGetObjsOutputFile);
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line;
		while ((line = br.readLine()) != null) {
			// process the line.
			if (line.startsWith("OUTPUTLINE:")) {
				String[] parts = line.split(":");
				if (parts.length!=3) throw new Exception("Unrecognised output line: " + line);
				return_value.put(parts[2].trim(),parts[1].trim());
		   };
		}
		return return_value;
	};
	
	/*
		Used by LoadSQLFileList
		 this scans the directory it is given and adds SQLFiles to the List
	*/
	private static void ScanDirectoryForSQLFiles(String p_code_dir, SQLFileType p_sqlfiletype, List<SQLFile> p_listOfFiles) throws Exception {
		File f = new File(p_sqlfiletype.getDirectory(p_code_dir));
		if (f.exists()) { //Directory may not exist when we are deploying from resource jar
			File[] listOfFiles = f.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (p_sqlfiletype.isSourceFileForThisType(listOfFiles[i].getName())) {
					p_listOfFiles.add(new SQLFile(listOfFiles[i].getName(), p_sqlfiletype, p_code_dir));
				}
			}
		}
	}
	
	private static List<SQLFile> LoadSQLFileList(String p_sqlSourceDirectory) throws Exception {
		List<SQLFile> return_value = new ArrayList<SQLFile>();
		
		ScanDirectoryForSQLFiles(p_sqlSourceDirectory, SQLFileType.PACKAGEHEAD, return_value);
		ScanDirectoryForSQLFiles(p_sqlSourceDirectory, SQLFileType.PACKAGEBODY, return_value);
		ScanDirectoryForSQLFiles(p_sqlSourceDirectory, SQLFileType.FUNCTION, return_value);
		ScanDirectoryForSQLFiles(p_sqlSourceDirectory, SQLFileType.PROCEDURE, return_value);
		ScanDirectoryForSQLFiles(p_sqlSourceDirectory, SQLFileType.TRIGGER, return_value);
		ScanDirectoryForSQLFiles(p_sqlSourceDirectory, SQLFileType.VIEW, return_value);
		
		
		return return_value;
	}

	/*
		Force an object to change
		 This is done by resetting the DB hash code so it will never match
		 UNLESS the code is already null. In this case it needs to remain null
		  as this is how the process knows to use insert or update the schema objects records
	*/
	private static void ForceChange(
		String p_RelativeFileName,
		Map<String,String> p_fileHashes
	) {
		if (null==p_fileHashes.get(p_RelativeFileName)) return; //No action required
		p_fileHashes.put(p_RelativeFileName,"FORCECHANGE");	
	};
	
	private static void EnsurePackagaHeadAndBodyChangeTogether(
		List<SQLFile> p_sqlFiles,
		Map<String,String> p_fileHashes
	) throws Exception {
		for (int c=0;c<p_sqlFiles.size();c++) {
			if (p_sqlFiles.get(c).hasChanged(p_fileHashes)) {
				switch(p_sqlFiles.get(c).getType()) {
				case PACKAGEHEAD:
					String BodyRelativeFile = SQLFileType.PACKAGEBODY.getRelativeDirectory() + "/" + p_sqlFiles.get(c).getObjectName() +SQLFileType.PACKAGEBODY.getFileNameTerminator();
					ForceChange(BodyRelativeFile, p_fileHashes);
					break;
				case PACKAGEBODY:
					String HeadRelativeFile = SQLFileType.PACKAGEHEAD.getRelativeDirectory() + "/" + p_sqlFiles.get(c).getObjectName() +SQLFileType.PACKAGEHEAD.getFileNameTerminator();
					ForceChange(HeadRelativeFile, p_fileHashes);
					break;
				}
			}
		}
	}
	
	//Return true if it was needed in the output, false otherwise
	private static boolean OutputAllSQLFile(
		SQLFile p_fil, 
		SQLFileWriter p_sqlFileWriter, 
		String p_sqlobjectdeployer_OBJECT_TABLE, 
		String p_schema, 
		Map<String,String> p_fileHashes,
		String p_code_dir,
		String p_version
	) throws Exception {
		if (!p_fil.hasChanged(p_fileHashes)) {
			return false;
		}

		String SchemaOBjectTableSQL = "";
		
		if (null==p_fileHashes.get(p_fil.getRelativeFileName())) {
			//Insert line into table
			SchemaOBjectTableSQL="insert into " + p_schema + ".\"" + p_sqlobjectdeployer_OBJECT_TABLE + "\" (\"hash\",\"file_name\",\"version\",\"deployment_order\") values ('" + p_fil.getMD5Hash().trim() + "','" + p_fil.getRelativeFileName() + "','" + p_version + "','" + p_fil.getSortKey() + "');";
		} else {
			//Update the line that is already in the table
			SchemaOBjectTableSQL="update " + p_schema + ".\"" + p_sqlobjectdeployer_OBJECT_TABLE + "\" set \"hash\"='" + p_fil.getMD5Hash().trim() + "', \"version\"='" + p_version + "', \"deployment_order\"='" + p_fil.getSortKey() + "' where \"file_name\"='" + p_fil.getRelativeFileName() + "';";
		}

		String sql = "";
		sql += "PROMPT Deploying " + p_schema + "." + p_fil.getObjectName() + "\n";
		sql += "@" + p_code_dir + p_fil.getRelativeFileName() + "\n";
		sql += "/\n";
		sql += "CLEAR BUFFER;\n";
		sql += "set serveroutput on;\n";
		sql += "execute " + p_schema + ".SCHEMA_DISPLAY_ERRORS ('" + p_schema + "','" + p_fil.getObjectName() + "'," + p_fil.getType().getTypeForErrorCheck() + ");\n";
		sql += "set serveroutput off;\n";
		sql += "CLEAR BUFFER;\n";
		sql += SchemaOBjectTableSQL + "\n";
		sql += "commit;";

		p_sqlFileWriter.Write(sql);
		
		return true;
	}
	
	private static void OutputAllSQLFiles(
		List<SQLFile> p_sqlFiles, 
		SQLFileWriter p_sqlFileWriter, 
		Map<String,String> p_fileHashes, 
		String p_sqlobjectdeployer_OBJECT_TABLE, 
		String p_schema,
		String p_code_dir,
		String p_version
	) throws Exception {
		for (int c=0;c<p_sqlFiles.size();c++) {
			//System.out.println("HH:" + p_fileHashes.get(p_sqlFiles.get(c).getRelativeFileName()));
			if (
				OutputAllSQLFile(
					p_sqlFiles.get(c), 
					p_sqlFileWriter,
					p_sqlobjectdeployer_OBJECT_TABLE,
					p_schema,
					p_fileHashes,
					p_code_dir,
					p_version
				)
			) {
				//File was output, remove it from HASH map to register it has been processed
				// files remaining in HASH map will be deleted
				System.out.println("To Run " + p_sqlFiles.get(c).getRelativeFileName());
			};
			//Remove the hash as the file was there irrespective if it was deployed
			// otherwise the object will be dropped
			p_fileHashes.remove(p_sqlFiles.get(c).getRelativeFileName());
		}
	};
	
	/*
	We have some deployed objects left in the database that didn't match any file in source control(records in p_fileHashes)
	These represent deleted objects
	We need to drop all these objects
	*/
	private static class objectToDelete implements Comparable<objectToDelete> {
		private String m_objName;
		private String m_fileName; //exact file name held so we can sellect correct schema_objects row to delete
		private SQLFileType m_typ;
		public objectToDelete(String a, SQLFileType b, String c) {m_objName= a; m_typ=b; m_fileName=c;};
		public String getObjName() {return m_objName;};
		public SQLFileType getTyp() {return m_typ;};
		public String getSortKey() {return m_typ.getSortKey();};
		public String getFileName() {return m_fileName;};
		@Override
		public int compareTo(objectToDelete p_o) {
			return getSortKey().compareTo(p_o.getSortKey());
		}			
	}
	private static void OutputDeleteActions(
		SQLFileWriter p_sqlFileWriter, 
		Map<String,String> p_fileHashes, 
		String p_sqlobjectdeployer_OBJECT_TABLE, 
		String p_schema
	) throws Exception {
		List<objectToDelete> objectsToDelete = new ArrayList<objectToDelete>();
		
		for (Map.Entry<String, String> entry : p_fileHashes.entrySet()) {
			SQLFileType fileType = SQLFileType.getTypeFromSourcefile(entry.getKey());
			if (null==fileType) throw new Exception("File type not found for deployed object " + entry.getKey());
			String curFileName = entry.getKey().substring(fileType.getRelativeDirectory().length()+1);
			String objectName = fileType.getObjectName(curFileName);
			//If we are dropping a package head make sure we drop the body
			if (fileType==SQLFileType.PACKAGEHEAD) {
				if (null==p_fileHashes.get(SQLFileType.PACKAGEBODY.getRelativeDirectory() + "/" + objectName +SQLFileType.PACKAGEBODY.getFileNameTerminator())) throw new Exception("Going to drop package head but not body - " + entry.getKey());
			};
			//If we are dropping a package body make sure we drop the head
			if (fileType==SQLFileType.PACKAGEBODY) {
				if (null==p_fileHashes.get(SQLFileType.PACKAGEHEAD.getRelativeDirectory() + "/" + objectName +SQLFileType.PACKAGEHEAD.getFileNameTerminator())) throw new Exception("Going to drop package body but not head - " + entry.getKey());
			};
			objectsToDelete.add(new objectToDelete(objectName,fileType,entry.getKey()));
		}
		
		//sort into deletion order (opposite of deploy order)
		Collections.sort(objectsToDelete, Collections.reverseOrder());
		
		for (int c=0;c<objectsToDelete.size();c++) {
			System.out.println("Dropping " + objectsToDelete.get(c).getTyp() + " " + objectsToDelete.get(c).getObjName());
			String sql = "";
			sql += objectsToDelete.get(c).getTyp().getDropString(objectsToDelete.get(c).getObjName()) + "\n";
			sql += "delete from " + p_schema + ".\"" + p_sqlobjectdeployer_OBJECT_TABLE + "\" where \"file_name\"='" + objectsToDelete.get(c).getFileName() + "';\n";
			sql += "commit;";

			p_sqlFileWriter.Write(sql);
		};
	}
	
	/*
		Debug function for outputing hash data
	*/
	private static void outputMap(Map<String,String> p_map) throws Exception {
		for (Map.Entry<String, String> entry : p_map.entrySet()) {
			System.out.println(entry.getKey() + " has hash " + entry.getValue());
		}
	}
	/*
		Debug function for outputing file data
	*/
	private static void outputList(List<SQLFile> p_list) throws Exception {
		for (int c=0;c<p_list.size();c++) {
			System.out.println(c + "." + p_list.get(c).toStringX());
		};
	};
	
	public static void main(String[] args) {
		try {
			if (args.length != 6) throw new Exception("Wrong number of arguments");
			String sqlSourceDirectory = args[0];
			String sqldeveloperGetObjsOutputFile = args[1];
			String sqlOutputDirectory = args[2];
			String sqlobjectdeployer_OBJECT_TABLE = args[3];
			String schema = args[4];
			String version = args[5];
			
			/*
			System.out.println("sqlSourceDirectory=" + sqlSourceDirectory);
			System.out.println("sqldeveloperGetObjsOutputFile=" + sqldeveloperGetObjsOutputFile);
			System.out.println("sqlOutputDirectory=" + sqlOutputDirectory);
			System.out.println("sqlobjectdeployer_OBJECT_TABLE=" + sqlobjectdeployer_OBJECT_TABLE);
			System.out.println("schema=" + schema);
			System.out.println("version=" + version);
			*/
			
			//Check all package bodies have headers
			//Check all package sources have bodies
			CheckAllPackageBodiesAndHeadersMatch(sqlSourceDirectory);
			
			//Read Hashes from file
			//Read in all files
			Map<String,String> fileHashes = ReadDBHashesSQLPlusOutputFile(sqldeveloperGetObjsOutputFile);
			
			//Read files into array
			List<SQLFile> sqlFiles = LoadSQLFileList(sqlSourceDirectory);
			if (sqlFiles.size()==0) {
				System.out.println(" warning - Could not find any files to deploy (Dir searched:" + sqlSourceDirectory + ")");
			};
			
			//Sort files into correct order
			Collections.sort(sqlFiles);

			SQLFileWriter sqlFileWriter = new SQLFileWriter(sqlOutputDirectory,schema);
			
			//Make sure Package Headers and Bodies are both changed if either has changed
			EnsurePackagaHeadAndBodyChangeTogether(sqlFiles, fileHashes);
			//outputMap(fileHashes); //Debug line can be used to view hashes that have FORCECHANGE set
			
			//Output all files
			// - if outputing package source FORCE outputing body by resetting the hash
			OutputAllSQLFiles(sqlFiles, sqlFileWriter, fileHashes, sqlobjectdeployer_OBJECT_TABLE, schema, sqlSourceDirectory, version);
			
			if (version.contains("SNAPSHOT")) {
				System.out.println("Deploying snapshot version - skipping object deletes");
			} else {
				//Output DELETE actions
				OutputDeleteActions(sqlFileWriter, fileHashes, sqlobjectdeployer_OBJECT_TABLE, schema);
			}
			sqlFileWriter.close();
			
			//outputList(sqlFiles);
			//outputMap(fileHashes);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		};
	}
}