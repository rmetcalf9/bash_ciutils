# Development machine setup instructions

This will work with a oracle database. I used http://www.oracle.com/technetwork/database/enterprise-edition/databaseappdev-vm-161299.html to give myself a local oracle database to work with. With an Oracle 12C database it is useful to create pluggable databases which can be easily destoryed and recreated. My rough notes for this are here https://code2.metcarob.com/node/284.

 1. Clone this repo onto the development machine
 2. Set an enviroment variable CIUTILS_HOME to point to the clone you made in step 1
 3. (Windows) create c:\bats directory and add it to system path
 4. Run install.bat or install.sh - this will setup the commands that maven needs
    TEST - run "ci_deployer" from the command prompt. The script should run and complain about having no args
 5. Create a directory where you will store your enviroment files. (These will hold DB connection details and passwords)
    I chose to use "/home/robert/cienv"
    [Enviroment](docs/ENVIROMENTS.md) will explain what files to place in this directory and what structure to use
    Fill this directory with required properties file.
 6. Set an enviroment variable CIICENV_HOME to point to the directory structure you just set up
 7. Install SQL Plus on development machine. (You can test by making sure typing sqlplus at the command prompt works)
    Test SQLPLUS:
````
sqlplus CIDBADMIN/CIDBADMIN@localhost:1521/CITESTDB_001 as sysdba
````
 8. Register object store with maven
You will need a connection to the object store in order for maven to retrieve and upload archives ready for deployment. The sample schemas use my personal object store and won't work unless you have access to it. I use private keys. First find your maven .m2 directory and add a settings.xml file:
````
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
      <localRepository/>
      <interactiveMode/>
      <usePluginRegistry/>
      <offline/>
      <pluginGroups/>
      <servers>
          <server>
              <id>ssh-snapshot-repository</id>
               <username>maven_repo</username>
          </server>
          <server>
              <id>ssh-repository</id>
               <username>maven_repo</username>
          </server>
      </servers>
      <mirrors/>
      <proxies/>
      <profiles>
            <profile>
                  <activation>
                        <activeByDefault>true</activeByDefault>
                  </activation>
                  <repositories/>
            </profile>
      </profiles>
      <activeProfiles/>
</settings>
````
change maven_repo to point to the user of the maven. In the above example I am using the username maven_repo. Your object store may have a different username. I also am using ssh keys to authenticate. This enables me to avoid supplying a password here in this file.
 9. Connect to main object store via ssh at least once: 'ssh maven_repo@mvn.metcarob.com -p 7456' - this makes sure ssh has no errors and there is a record in the knwon_hosts file
 10. Connect to snapshot object store via ssh at least once: 'ssh maven_repo@mvnsnap.metcarob.com -p 7456' - this makes sure ssh has no errors and there is a record in the knwon_hosts file

# Running a sample database deployment process

To demo that everything is working you can deploy the two sample schemas to a database. Before you do this create a new empty database. You will need a system user for this database with lots of privillages. https://code2.metcarob.com/node/284 shows an example creation with a list of required privalalges. The sysdba role will be required.

In the following example my enviroment will be rjmdev

## Clone sample repos
 1. Clone git@gitlab.com:rmetcalf9/ICSchema_SAMPLE_SCHEMA.git
 2. Clone git@gitlab.com:rmetcalf9/ICSchema_SAMPLE_UTIL_SCHEMA.git

## Setup enviroment file

Go to the CIICENV_HOME directory
Create a subdirectory for the enviroment you are setting up. I am using "rjmdev"
In this directory create a file called "soa.properties" - The sample repos are setup to install to the soa system.
For my enviroment I added the following to this file
````
##rjmdev connection details for soa system


ic.db.host=localhost
ic.db.port=1521
ic.db.ciuser=CIDBADMIN
ic.db.ciuserpass=CIDBADMIN
ic.db.service_name=CITESTDB_001
````

## Execute deploymeny
Enter the directory for ICSchema_SAMPLE_SCHEMA and run the command 
````
mvn deploy -Dcmd.env=rjmdev
````
