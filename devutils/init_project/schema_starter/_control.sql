set serveroutput on SIZE UNLIMITED;
WHENEVER SQLERROR EXIT SQL.SQLCODE;

PROMPT Start of pre migration control script
PROMPT Schema Name: &1

@create_schema_and_tablespace.sql &1

PROMPT End of pre migration control script

exit;