declare
	l_script_name varchar2(255) := 'create_schema_and_tablespace.sql';
  l_numSchemas number;
  l_numTableSpaces number;
  l_systemSchemaFileName varchar2(4096);
  l_newSchemaFileName varchar2(4096);
  l_lastPathSeperator number;
  
  l_schema_name varchar2(255) := '&1';
begin
	dbms_output.put_line('Start of ' || l_script_name || ' ' || l_schema_name);
  
  select count('x')
  into l_numTableSpaces
  from USER_TABLESPACES
  where tablespace_name=l_schema_name 
  ;
  if (l_numTableSpaces=0) then 
    --caculate schema file name:
    select file_name
    into l_systemSchemaFileName
    from DBA_DATA_FILES
    where TABLESPACE_NAME='SYSTEM'
    ;
    l_lastPathSeperator := instr(l_systemSchemaFileName,'/',-1);
    if nvl(l_lastPathSeperator,0)=0 then
      l_lastPathSeperator := instr(l_systemSchemaFileName,'\',-1);
    end if;
    l_newSchemaFileName := substr(l_systemSchemaFileName,0,l_lastPathSeperator) || l_schema_name || '.dbf';
    
  	dbms_output.put_line('Tablespace not found creating ' || l_newSchemaFileName);
    
    
    execute immediate 'create tablespace ' || l_schema_name || ' DATAFILE ''' || l_newSchemaFileName || ''' size 40M';
  end if;   
   
  select count('x')
  into l_numSchemas
  from all_users
  where username=l_schema_name
  ;
  
  if (l_numSchemas=0) then
  	dbms_output.put_line('User not found creating');
    execute immediate 'create USER ' || l_schema_name || ' IDENTIFIED BY ' || l_schema_name || ' DEFAULT TABLESPACE ' || l_schema_name;
    execute immediate 'ALTER USER ' || l_schema_name || ' quota unlimited on ' || l_schema_name;
	execute immediate 'grant create session to ' || l_schema_name;
	
  end if;


	dbms_output.put_line('End of ' || l_script_name);

end;
/
CLEAR BUFFER;

