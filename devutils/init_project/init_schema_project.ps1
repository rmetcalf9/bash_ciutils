$ErrorActionPreference = "Stop"

$dir_to_init = (Get-Item -Path ".\" -Verbose).FullName

$directoryInfo = Get-ChildItem $dir_to_init | Measure-Object
If ( $directoryInfo.count -ne 0 ) {
    Write-Host "Must be run from the directory you wish to use for the schema project and this must be a new empty directory."
    pause
    exit 1
}


Write-Host "This will init a schema workspace in the location:" $dir_to_init

#dir $env:CIUTILS_HOME/devutils/init_project/schema_starter

New-Item -ItemType Directory -Force -Path $dir_to_init/src/test/resources/db/code
New-Item -ItemType Directory -Force -Path $dir_to_init/src/test/java
New-Item -ItemType Directory -Force -Path $dir_to_init/src/main/resources/db/code/Functions
New-Item -ItemType Directory -Force -Path $dir_to_init/src/main/resources/db/code/Packages
New-Item -ItemType Directory -Force -Path $dir_to_init/src/main/resources/db/code/Procedures
New-Item -ItemType Directory -Force -Path $dir_to_init/src/main/resources/db/code/Triggers
New-Item -ItemType Directory -Force -Path $dir_to_init/src/main/resources/db/code/Views
New-Item -ItemType Directory -Force -Path $dir_to_init/src/main/resources/db/migration
New-Item -ItemType Directory -Force -Path $dir_to_init/src/main/resources/db/pre_migration

copy $env:CIUTILS_HOME/devutils/init_project/schema_starter/.gitignore $dir_to_init
copy $env:CIUTILS_HOME/devutils/init_project/schema_starter/pom.xml $dir_to_init
copy $env:CIUTILS_HOME/devutils/init_project/schema_starter/_control.sql $dir_to_init/src/main/resources/db/pre_migration/_control.sql
copy $env:CIUTILS_HOME/devutils/init_project/schema_starter/create_schema_and_tablespace.sql $dir_to_init/src/main/resources/db/pre_migration/create_schema_and_tablespace.sql


echo "Complete - please update pom.xml before continuing"