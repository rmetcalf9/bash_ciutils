@echo _
@echo Script to install ciutilities
@echo _

@SET LBATDIR=C:\BATS

@IF "%CIUTILS_HOME%"=="" goto error_CIUTILS_HOME_not_defined
@IF NOT EXIST %LBATDIR% GOTO error_BAT_dir_missing

@IF EXIST %LBATDIR%\init_schema_project.bat goto skip_001
@echo Writing init_schema_project.bat
@echo @PowerShell.exe -Command "%CIUTILS_HOME%\devutils\init_project\init_schema_project.ps1 %%1 %%2 %%3 %%4 %%5 %%6 %%7 %%8 %%9" > %LBATDIR%\init_schema_project.bat
:::@echo @exit %%ERRORLEVEL%% >> %LBATDIR%\init_schema_project.bat
:::DEV Script not run from MAVEN so dosen't have to exit (Exiting will close console)
:skip_001


@echo _
@echo Sucessful end of script
@goto end

:error_CIUTILS_HOME_not_defined
@ECHO ERROR - CIUTILS_HOME is NOT defined
@echo  - it needs to point to the local directory for CIUTILS
@goto end

:error_BAT_dir_missing
@ECHO ERROR - Could not find %LBATDIR% directory
@echo  - it must be created and added to system path
@goto end


:end
@pause