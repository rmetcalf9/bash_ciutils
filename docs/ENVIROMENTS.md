# Enviroments

An enviroment represents a group of systems used for a spercific purpose. e.g. DEV, TEST or PROD. An enviroment will consist of one or more systems. 

A system is treated as a single deployment target and will have a properties file with connection details passwords, etc.

![](repo_types.png?raw=true)

# Source Control Repos

Most schemas will be specially developed for a single system. E.g. the Datamart Schema. The pom file for this type of repo will contain a property ic.pom.sys which will define a system. This repo will only ever be deployed to this system. This system must match a properties file in the enviroment structure.

# Enviroment struture

Every machine that can run deployments will have an enviroment variable called CIICENV_HOME which will point to a directory structure which contains the properties used by maven to make deployments to each enviroment. It is important the structure is followed as when you specify

The structure should be
$CIICENV_HOME/**ENV**/**SYS**.properties

