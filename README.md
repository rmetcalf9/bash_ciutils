# Background
This project contains a set of utilities to support using maven to deploy Oracle database schemas.
A sample schema is https://gitlab.com/rmetcalf9/ICSchema_SAMPLE_SCHEMA

## Features

### General Features

 - Tools run on windows and linux. (batch files and bash scripts provided)
 - Mutiple enviroments configured in enviroment file
 - Dependant schemas specified in the POM file (Library schemas possible)
 - Maven based deployment
 - Deployment archive generation (kind of like creating a patch)
 - Direct deployment form object store (e.g. running on a CI server)
 - Deployment to object using maven release process

### Database Deployment Features (sqlcodedeploy)

 - Optional schema creation step (to support both dev and prod installs)

### Stateful Database Object Deployment Features (ci_flyway)

 - Stateful object managemnt (ALTER TABLE, etc.)
 - Use of Flyway as part of the process to produce migration scripts (https://flywaydb.org/)
 - Record current version in "schema_version" table
 - Only apply required updates

### Non-Stateful Database Object Deployment Features (ci_deployer)

 - Overridable deployment order
 - Deploys only objects changed in each release (use of hash code)

### Testing Features (sqlplusmulti)

 - PLSQL Test runner


# Guides

 1. [Development enviroment setup](DEVSETUP.md)
 2. [Deploying schemas without the use of maven](DEPLOYMENT.md)



