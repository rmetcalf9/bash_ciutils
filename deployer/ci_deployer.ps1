﻿Param(
  [string]$one,
  [string]$two,
  [string]$three,
  [string]$four,
  [string]$five
)
$ErrorActionPreference = "Stop"

Write-Host ""
Write-Host "Start of deployer powershell script"

if (-not (Test-Path env:CIUTILS_HOME)) { 
  Write-Host "CIUTILS_HOME enviroment var not set"
  Write-Host "This should be set to the location where you checked out the ciutils project to"
  Write-Host "(git@gitlab.com:rmetcalf9/bash_ciutils.git)"
  exit 1
}

##Compile java if required
if (-not (Test-Path $env:CIUTILS_HOME/deployer/java/Main.class)) { 
    cd $env:CIUTILS_HOME/deployer/java
    javac -cp $env:CIUTILS_HOME/deployer/java`;$env:CIUTILS_HOME/deployer/java/jlib/flyway-core-4.0.3.jar Main.java
    if ( ! ( $? )) {
	    echo "ERROR Failed to compile"
	    exit 1
    }
    if (-not (Test-Path $env:CIUTILS_HOME/deployer/java/Main.class)) { 
        echo "ERROR Failed to find .class file"
        exit 1
    }
}

java -cp $env:CIUTILS_HOME/deployer/java/jlib/flyway-core-4.0.3.jar`;$env:CIUTILS_HOME/deployer/java/jlib/ojdbc7.jar`;$env:CIUTILS_HOME/deployer/java Main "$one" "$two" "$three" "$four" "$five"
if ( ! ( $? )) {
	exit 1
}


Write-Host "End of deployer powershell script"
Write-Host ""

exit 0