import org.flywaydb.core.Flyway;


/*
Simple program that just runs flyway based on it's inputs
needed since we can't use flyway plugin directly from pom with paramater evaluation
*/

public class Ci_flyway {
	/*
		                <argument>"${ic.db.jdbc.connectstring}"</argument>
		                <argument>"${ic.db.ciuser}"</argument>
		                <argument>"${ic.db.ciuserpass}"</argument>
		                <argument>"${ic.db.schema.name}</argument>
	
	*/
	public static void main(String[] args) {
		try {
			if (args.length != 5) throw new Exception("Incorrect number of arguments - " + args.length);
			
			String jdbc_connectstring = DeploymentRunnerOracleDB.RemoveSurroundingDoubleQuotesIfPresent(args[0]);
			String ciuser = DeploymentRunnerOracleDB.RemoveSurroundingDoubleQuotesIfPresent(args[1]);
			String ciuserpass = DeploymentRunnerOracleDB.RemoveSurroundingDoubleQuotesIfPresent(args[2]);
			String schema_name = DeploymentRunnerOracleDB.RemoveSurroundingDoubleQuotesIfPresent(args[3]);
			String migration_dir = DeploymentRunnerOracleDB.RemoveSurroundingDoubleQuotesIfPresent(args[4]);

			String[] locations = new String[1];
			locations[0] = "filesystem:" + migration_dir;
			String[] schemas = new String[1];
			schemas[0] = schema_name;
			
			System.out.println("Flymigration dir=" + migration_dir);
			
			DeploymentRunnerOracleDB.ExecuteFlywayMigrate(locations, schemas,jdbc_connectstring,ciuser,ciuserpass);

		} catch (Exception e3) {
			e3.printStackTrace();
			System.exit(1);
		};
	};
};
