import java.util.List;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder;
import java.io.File;
/*
Class that will execute a shell command
Display it's output as part of the normal output
check exit status and throw an error
*/
public class Shell {
	private String m_working_dir = "";
	private String m_cmd = "";
	private List<String> m_arguments = null;
	private long m_timeout = 0;
	
	public Shell(String p_working_dir, String p_cmd, List<String> p_arguments, long p_timeout) throws Exception {
		m_working_dir = p_working_dir;
		m_cmd = p_cmd;
		m_arguments = p_arguments;
		m_timeout = p_timeout;
		
		File f = new File(m_working_dir);
		if (!f.exists()) throw new Exception("ERROR Working directory for command " + p_cmd + " dosen't exist - " + m_working_dir);
	};
	
	
	
	public void execute() throws Exception {
		String[] buf = null;
		if (OSValidator.isWindows()) {
			buf = new String[3];
			String tmp = m_cmd;
			for (int c=0;c<m_arguments.size();c++) {
				tmp += " " + m_arguments.get(c);
			};

			buf[0] = "CMD";
			buf[1] = "/C";
			buf[2] = "\"" + tmp + "\"";
		} else {
			buf = new String[m_arguments.size() + 1];
			
			buf[0] = m_cmd;
			for (int c=0;c<m_arguments.size();c++) {
				buf[c+1] = m_arguments.get(c);
			}			
		}
		

		Torun torun = new Torun(buf,m_working_dir);
		Thread t = new Thread(torun);
		t.start();
	    long start = System.currentTimeMillis();
		while (t.isAlive()) {
			//Do nothing
			//System.out.println(System.currentTimeMillis() + ":" + (start+m_timeout));
			if (System.currentTimeMillis()>(start+m_timeout)) {
				t.stop(); //Thread calls readbuffer which hangs so I can't use interrupt here
				throw new Exception("Command timeout");
			}
			//System.out.print(".");
		}

		torun.exceptionTest(); //throw any exceptions		
		
		//System.out.println("TODORUN in WD " + m_working_dir);
		//System.out.println("CMD " + m_cmd);
		//for (int c=0;c<m_arguments.size();c++) {
		//	System.out.println(" 1:" + m_arguments.get(c));
		//};
	}

	private class Torun implements Runnable {
		private String m_working_dir = "";
		private String m_Command[] = null;
		private String m_Output = "";
		private Exception m_Exception = null;
		public Torun(String[] p_Command, String p_working_dir) {
			m_Command = p_Command;
			m_working_dir = p_working_dir;
		}
			
		@Override
		public void run() {
			try {
				m_Output = "";
				
				Process p = null;
				ProcessBuilder pb = new ProcessBuilder(m_Command);
				pb.redirectErrorStream(true);
				pb.directory(new File(m_working_dir));
				p = pb.start();

				InputStreamReader isr = new InputStreamReader(p.getInputStream());
			    BufferedReader buf = new BufferedReader(isr);
			    StringBuffer res = new StringBuffer();
			    
				String line = "";			
				while ((line = buf.readLine())!= null) {
					res.append(line + "\n");
					System.out.println(line);
				}
				m_Output = res.toString();
				
				p.waitFor();
				
				int exitStatus = p.exitValue();
				if (0!=exitStatus) throw new Exception("Process error (exit status=" + exitStatus + ")");

			} catch (Exception e) {
				m_Exception = e;
			}
		}
		
		//Called in main thread to report exception
		public void exceptionTest() throws Exception {
			if (m_Exception!=null) throw m_Exception;
		}
		
		public String getOutput() {return m_Output;};
		
	}	
};