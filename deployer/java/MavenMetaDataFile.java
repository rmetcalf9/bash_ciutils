import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.namespace.NamespaceContext;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.net.URL;

/*
Class for reading a POM file
*/
public class MavenMetaDataFile {
	private String m_Snapshot_timestamp = "";
	private String m_Snapshot_buildnumber = "";
	private String m_actualversion = "";
	private String m_GroupID = "";
	private String m_ArtifactID = "";
	private String m_Version = "";
	private String m_repoURL = ""; //Repo that this metadata file came from
	private boolean m_localREPO = false;

	private String m_relative_path = ""; //Caculated 
	// Directory that the files are stored on
	//Example: metcarob/com/oracledb/sample/schema/ICSchema_SAMPLE_UTIL_SCHEMA/0.0.1-SNAPSHOT
	
	public static final String filename="maven-metadata.xml";
	public static final String localfilename="maven-metadata-local.xml";
	
	/*
	Used for release - there is no metadata in this case just a pom file.
	*/
	public MavenMetaDataFile(
		String p_repoURL,
		String p_GroupID,
		String p_ArtifactID,
		String p_Version
	) throws Exception {
		m_localREPO = false;
		m_repoURL = p_repoURL;
		
		m_Snapshot_timestamp = "";
		m_Snapshot_buildnumber = "";

		m_GroupID = p_GroupID;
		m_ArtifactID = p_ArtifactID;
		m_Version = p_Version;
		
		m_actualversion = m_Version;
		
		m_relative_path = getGroupID().replace(".","/") + "/" + getArtifactID().replace(".","/") + "/" + getVersion();
		
	};
	
	
	/*
	Used from Snapshot
	*/
	public MavenMetaDataFile(File p_input, String p_repoURL, boolean p_localREPO) throws Exception {
		Document doc = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		
		m_repoURL = p_repoURL;
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(false);
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(p_input);
		m_localREPO = p_localREPO;
		
		
		/*Example: 
<metadata modelVersion="1.1.0">
	<groupId>metcarob.com.oracledb.sample.schema</groupId>
	<artifactId>ICSchema_SAMPLE_SCHEMA</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<versioning>
		<snapshot>
			<timestamp>20160908.180657</timestamp>
			<buildNumber>2</buildNumber>
		</snapshot>
		<lastUpdated>20160908180657</lastUpdated>
		<snapshotVersions>
			<snapshotVersion>
				<extension>jar</extension>
				<value>0.0.1-20160908.180657-2</value>
				<updated>20160908180657</updated>
			</snapshotVersion>
			<snapshotVersion>
				<extension>pom</extension>
				<value>0.0.1-20160908.180657-2</value>
				<updated>20160908180657</updated>
			</snapshotVersion>
		</snapshotVersions>
	</versioning>
</metadata>
*/

		if (!doc.getDocumentElement().getNodeName().equals("metadata")) throw new Exception("Not a metadata file - no project root element (" + doc.getDocumentElement().getNodeName() + ")");

		m_Version = xPath.evaluate("/metadata/version",doc.getDocumentElement());
		m_Snapshot_timestamp = xPath.evaluate("/metadata/versioning/snapshot/timestamp",doc.getDocumentElement());
		m_Snapshot_buildnumber = xPath.evaluate("/metadata/versioning/snapshot/buildNumber",doc.getDocumentElement());
		
		if (!m_Version.contains("SNAPSHOT")) throw new Exception("Dealing with real versions Not implemented");
		
		m_actualversion = m_Version.replace("SNAPSHOT",m_Snapshot_timestamp + "-" + m_Snapshot_buildnumber);
		
		m_GroupID = xPath.evaluate("/metadata/groupId",doc.getDocumentElement());
		m_ArtifactID = xPath.evaluate("/metadata/artifactId",doc.getDocumentElement());
		
		m_relative_path = getGroupID().replace(".","/") + "/" + getArtifactID().replace(".","/") + "/" + getVersion();

		if (p_localREPO) {
			//File name in the local repo have -SNAPSHOT at the end
			m_actualversion = m_Version;
		} else {
			m_actualversion = m_Version.replace("SNAPSHOT",m_Snapshot_timestamp + "-" + m_Snapshot_buildnumber);
		}
		
	}
	
	public String getGroupID() {return m_GroupID;};
	public String getArtifactID() {return m_ArtifactID;};
	public String getVersion() {return m_Version;};

	public String getRepo() {
		return m_repoURL;
	};
	
	public String getRelativeFN() {
		return getRelativePath() + "/" + getArtifactID() + "-" + m_actualversion;
	};
	public String getRelativePath() {
		return m_relative_path;
	};
	
	public String getWebPath() {
		return m_repoURL + "/" + getRelativeFN();
	};
	
	private PomFile m_pomFileObj = null;
	public PomFile getPomFile(File p_tmpDirBase) throws Exception {
		if (null!=m_pomFileObj) return m_pomFileObj;
		
		if (m_localREPO) {
			return new PomFile(new File(getWebPath() + ".pom"));
		};
		
		String localPomFileName = p_tmpDirBase.getAbsolutePath() + "/" + getRelativeFN() + ".pom";
		if (!DownloadUtils.downloadFromUrl(new URL(getWebPath() + ".pom"),localPomFileName)) throw new Exception("Error getting pom file");
		File pf = new File(localPomFileName);
		if (!pf.exists()) throw new Exception("Pom file dosen't exist");
		m_pomFileObj = new PomFile(pf);
		return m_pomFileObj;
	};
	
	public boolean isLocal() {return m_localREPO;};
}