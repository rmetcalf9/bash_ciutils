import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collections;
import java.io.PrintWriter;
import java.io.FileInputStream;
import java.net.URL;

/*
deployer - this program is designed to deploy all the dependancies of a given artifact - from the repository.

ArtifactMode - deploy this artifact AND all it's dependancies.
Params:
GroupID
ArtifactID
Version
RepoURL
Enviroment (cmd.env)
Command Line System (cmd.parent.sys)

POMDependancyMode - deploy only dependiencies listed in the provided POM file (Use Repo's from pom file)
This mode is for use when run through Maven. The local maven repository locaiton is supplied as and this 
is used to retrieve SNAPSHOT artifacts.
Params:
pomfile
maven_repo_location (or NULL)
Enviroment (cmd.env)
Command Line System (cmd.parent.sys)
*/

public class Main {
	private static boolean m_searchSnapshotRepos = false;
	private static boolean m_searchMainRepos = true;
	private static boolean m_searchLocalRepos = false;
	
	private static String m_snapShotRepo = null;
	
	/*
	We will keep a list of all the pom files we have deployed.
	 If we try and deploy the same one once we error (prevents infinite loop when we get circular dependancies)
	*/
	private static Map<String,String> m_deployedPOMFiles = new HashMap<String,String>();
	
	/*
	This is called once for each dependancy in the pomFile
	 (Pom file is the parent pom file for the dependancy)
	 duplicate checking is irrespective of version
	*/
	public static void DeployArtifactFromRepo(
		PomFile.Dependancy p_depdancy,
		String p_cmd_env, 
		String p_cmd_parent_sys,
		String p_parentSys,
		String p_ic_snapshotURL,
		String p_ic_mainURL
	) throws Exception {
		System.out.println("Deploy " + p_depdancy.getGroupID() + ":" + p_depdancy.getArtifactID() + ":" + p_depdancy.getVersion());
		
		if (!m_searchSnapshotRepos) {
			if (p_depdancy.isSnapshot()) throw new Exception("SNAPSHOT dependancy not allowed");
		}
		
		//Note this is irrespective of version
		String duplicateCheckString = p_depdancy.getGroupID() + "_:_" + p_depdancy.getArtifactID();
		if (m_deployedPOMFiles.get(duplicateCheckString)!=null) throw new Exception("Deploying same artifact twice - do we have circular dependancies");
		m_deployedPOMFiles.put(duplicateCheckString,duplicateCheckString);
		
		
		//Download the POM file into TMP area
		MavenMetaDataFile meta = null;
		
		if (m_searchLocalRepos) {
			meta = DownloadUtils.getLocalMetaDataFile(
				m_snapShotRepo,
				p_depdancy.getGroupID(),
				p_depdancy.getArtifactID(),
				p_depdancy.getVersion(),
				m_tmpDirBase.getAbsolutePath()
			);
		};
		if (null==meta) {
			if (m_searchSnapshotRepos) {
				meta = DownloadUtils.getMetaDataFile(
					p_ic_snapshotURL,
					p_depdancy.getGroupID(),
					p_depdancy.getArtifactID(),
					p_depdancy.getVersion(),
					m_tmpDirBase.getAbsolutePath()
				);
			};
			if (null==meta) {
				if (m_searchMainRepos) {
					//In release repos there is no mavenmetadata file
					// we can go direct to pom
					meta = new MavenMetaDataFile(
						p_ic_mainURL,
						p_depdancy.getGroupID(),
						p_depdancy.getArtifactID(),
						p_depdancy.getVersion()
					);			
					//Create the temporary resource directory
					// This is normally done in DownloadUtils
					File fff = new File(m_tmpDirBase.getAbsolutePath() + "/" + meta.getRelativePath());
					fff.mkdirs();
				};
				if (meta!=null) {
					System.out.println(" getting reosurce from MAIN repo");
				};
			} else {;
				System.out.println(" getting reosurce from SNAPSHOT repo");
			}
		} else {
			if (meta != null) {
				System.out.println(" getting reosurce from Local repo");
			}
		};
		
		if (null==meta) throw new Exception("Couldn't locate a metadata file");
		
		//Download the pom file to a local location
		PomFile depPomFile = meta.getPomFile(m_tmpDirBase);
		
		//Work out actual parent system
		//If POM has value use that
		// otherwise use parent pom value
		String actual_parentSys = p_cmd_parent_sys;
		String parentPomSysProp = p_parentSys;
		if (parentPomSysProp != null) {
			if (!parentPomSysProp.equals("")) {
				if (!parentPomSysProp.equalsIgnoreCase("NULL")) {
					actual_parentSys = parentPomSysProp;
				};
			};
		};

		//Go through that POM's demendancies
		POMDependancyMode(
			depPomFile.getSourceFileName(),
			p_cmd_env, 
			actual_parentSys
		);

		
		//Call sub deployment routine based on deployment type property
		String ci_deployment_type = "NULL";
		String ci_deployment_typeTMP = depPomFile.getProperty("ic.ci.deployment.type");

		//System.out.println(ci_deployment_typeTMP);
		if (ci_deployment_typeTMP!=null) {
			if (!ci_deployment_typeTMP.equals("")) {
				ci_deployment_type = ci_deployment_typeTMP;
			}
		};

		DeploymentRunner runner = null;
		
		if (ci_deployment_type.equals("NULL")) {
			System.out.println(" - No type for depedancy so skipping deploy");
		} else if (ci_deployment_type.equals("OracleDB")) {
			runner = new DeploymentRunnerOracleDB(depPomFile,m_tmpDirBase.getAbsolutePath() + "/" + meta.getRelativePath());
		} else {
			throw new Exception("Depandacy POM file ic.ci.deployment.type property set to unknown type (" + ci_deployment_type + ") ");
		};
		runner.assembleResourcesInTmpLocation(meta);
		runner.LoadVariables(p_cmd_env, actual_parentSys);
		runner.Deploy();
		
	};

	public static void ArtifactMode(
		String p_GroupID,
		String p_ArtifactID,
		String p_Version,
		String p_RepoURL,
		String p_cmd_env,
		String p_cmd_parent_sys
	) throws Exception {
		m_searchSnapshotRepos = false;
		
		System.out.println("Deployer artifact mode");
		System.out.println(" p_GroupID:    " + p_GroupID);
		System.out.println(" p_ArtifactID: " + p_ArtifactID);
		System.out.println(" p_Version:    " + p_Version);
		System.out.println(" p_RepoURL:    " + p_RepoURL);
		System.out.println("");
		
		//Check version is not a shapshot version
		String tmp = p_Version.toUpperCase();
		if (tmp.contains("SNAPSHOT")) throw new Exception("Deployer Artifact mode should only be used of release artifacts - not snapshots");

		//Get a POMFile - only works with release poms
		MavenMetaDataFile meta = new MavenMetaDataFile(
			p_RepoURL,
			p_GroupID,
			p_ArtifactID,
			p_Version
		);			
		//Create the temporary resource directory
		// This is normally done in DownloadUtils
		File fff = new File(m_tmpDirBase.getAbsolutePath() + "/" + meta.getRelativePath());
		fff.mkdirs();
		
		PomFile pf = meta.getPomFile(m_tmpDirBase);
		
		DeployArtifactFromRepo(
			pf.getAsDependancy(),   //pomfile to deploy
			p_cmd_env, 
			p_cmd_parent_sys, //parent sys
			pf.getProperty("ic.pom.sys"), //pom sys for this pom file
			"IC_SNAPSHOT not used",
			p_RepoURL
		);
		
	};
	public static void POMDependancyMode(String p_pomFile, String p_cmd_env, String p_cmd_parent_sys) throws Exception {

		File pomFile = new File(p_pomFile);
		if (!pomFile.exists()) throw new Exception(p_pomFile + " not found");
		PomFile pf = new PomFile(pomFile);

		//Only allow snapshot dependancies if this POM is a snapshot
		// (If this POM is a release then refuse to deploy dependancies that are snapshots
		m_searchSnapshotRepos = pf.isSnapshot();

		System.out.println("Deploying Dependancies for " + pf.toString());
		if (m_searchSnapshotRepos) System.out.println(" - including SNAPSHOT repos");
		//Loop through all dependancies and deploy them
		for (int c=0; c<pf.getNumOfDependancies(); c++) {
			if (!pf.getDependancy(c).getScope().equalsIgnoreCase("test")) {
				DeployArtifactFromRepo(
					pf.getDependancy(c), 
					p_cmd_env, 
					p_cmd_parent_sys, 
					pf.getProperty("ic.pom.sys"),
					pf.getRepositoryURL("ic_snapshot"),
					pf.getRepositoryURL("ic_main")
				);
			};
		};
		System.out.println("");
	};

	private static File m_tmpDirBase = new File(System.getProperty("java.io.tmpdir") + "/icutils_deployer");
	/*
	Deletes the temporay locaiton that was being used
	*/
	public static void clearTmp() throws Exception {
		if (m_tmpDirBase.exists()) {
			//System.out.println("TMP CLEAR:" + m_tmpDirBase.getAbsolutePath());
			DownloadUtils.DeleteDirectory(m_tmpDirBase);
			if (m_tmpDirBase.exists()) {
				System.out.println("tmp location=" + m_tmpDirBase.getPath());
				throw new Exception("Failed to delete the contents of the tmp location");
			};
		};
	};
	
	public static void main(String[] args) {
		try {
			System.out.println("");
			System.out.println("*********************************************");
			System.out.println("Start of dependency deployer");
			System.out.println("   This will deploy all dependencies");
			System.out.println("*********************************************");
			//System.out.println(" - temp location=" + m_tmpDirBase.getAbsolutePath());
			clearTmp();
			m_tmpDirBase.mkdir();
			
			if (args.length == 6) {
				ArtifactMode(
					args[0].replace("\"",""),
					args[1].replace("\"",""),
					args[2].replace("\"",""),
					args[3].replace("\"",""),
					args[4].replace("\"",""),
					args[5].replace("\"","")
				);
			} else if (args.length == 4) {
				m_snapShotRepo = null;
				if (null!=args[1]) {
					String tmp = args[1].replace("\"","");
					if (!tmp.equalsIgnoreCase("NULL")) {
						m_snapShotRepo = tmp;
						if (!(new File(m_snapShotRepo)).exists()) throw new Exception("Maven Repo dosen't exist - " + tmp);
					};
				};
				POMDependancyMode(
					args[0].replace("\"",""),
					args[2].replace("\"",""), //cmd.env
					args[3].replace("\"","") //cmd.parent.sys
				);
			} else {
				System.out.println(" - recieved " + args.length + " arguments");
				for (int c=0;c<args.length;c++) {
					System.out.println("   " + c + ":-" + args[c]);
				};
				
				if (args.length == 0) {
					System.out.println("No arguments passed");
					System.out.println(" to deploy an artifact from repository run with the following 6 arguments:");
					System.out.println(" 1. Group ID");
					System.out.println(" 2. Artifact ID");
					System.out.println(" 3. Version");
					System.out.println(" 4. repository URL");
					System.out.println(" 5. Enviroment (dev/test/prod/etc)");
					System.out.println(" 6. Parent System (NULL, SOA, etc.)");
				};
				
				throw new Exception("Wrong number of arguments");
			};
			
			
			System.out.println("*********************************************");
			System.out.println("End of dependency deployer");
			System.out.println("*********************************************");
			
			System.out.println("");
			
		} catch (Exception e) {
			e.printStackTrace();
			
			try {
				clearTmp();
			} catch (Exception e2) {
				//Do nothing
			};
			
			System.exit(1);
		};
		try {
			clearTmp();
			//System.out.println("*************TMP DELETE DISABLED****************");
		} catch (Exception e3) {
			e3.printStackTrace();
			System.exit(1);
		};
	}
}