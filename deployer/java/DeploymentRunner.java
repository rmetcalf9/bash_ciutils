import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.namespace.NamespaceContext;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.Enumeration;

/*
Class for running a Deployment file
*/
public abstract class DeploymentRunner {
	protected PomFile m_pom = null;
	protected String m_tmpLocation = "";
	
	private Map<String,String> m_variables = null;
	
	//Diaplaying values for debugging
	private void DisplayVars() {
		System.out.println("Replacement variables:");
		for (Map.Entry<String, String> entry : m_variables.entrySet()) {
			String key = entry.getKey();
			System.out.println(key + "=" + entry.getValue());
		};		
	};
	
	private String SubstituteVariable(String p_source,int p_callNum) throws Exception {
		p_callNum++;
		if (p_callNum>500) throw new Exception("Too many substitutions");
		int pos = p_source.indexOf("${");
		if (-1==pos) return p_source;
		String remain = p_source.substring(pos + 2);
		int remainpos = remain.indexOf("}");
		int another_var_start_pos = remain.indexOf("${");
		if (-1 != another_var_start_pos) {
			if (another_var_start_pos < remainpos) {
				//Another variable has started inside this one
				return p_source.substring(0,pos) + SubstituteVariable("${" + SubstituteVariable(p_source.substring(pos+2),p_callNum),p_callNum);
			};
		};
		String variable_name = remain.substring(0,remainpos);
		
		String looked_up_var = m_variables.get(variable_name);
		if (null==looked_up_var) throw new Exception("Property not set \"" + variable_name + "\"");
		return p_source.substring(0,pos) + SubstituteVariable(looked_up_var + remain.substring(remainpos+1),p_callNum);
	};

	protected String SubstituteVariable(String p_source) throws Exception {
		return SubstituteVariable(p_source,0);
	};
	
	public DeploymentRunner(PomFile p_pom, String p_tmpLocation) throws Exception {
		m_pom = p_pom;
		m_tmpLocation = p_tmpLocation;
		
		m_variables= new HashMap<String,String>(m_pom.getProperties());
		
	};
	
	public String GetResourceLocation() {
		return m_tmpLocation + "/resources";
	};
	
	/*
	Assmebles the resources
	 - for local just extract the files
	 - for remote then download and then extract
	*/
	public void assembleResourcesInTmpLocation(MavenMetaDataFile p_meta) throws Exception {
		if (p_meta.isLocal()) {
			SetLocalResourcePrefix(p_meta.getWebPath());
			Extract();
			return;
		};
		Download(p_meta.getWebPath());
		Extract();
	};
	
	private void LoadICPropertyFile(String p_file_name) throws Exception {
		Properties prop = new Properties();
		InputStream input = null;		
		input = new FileInputStream(p_file_name);
		prop.load(input);		
		
		Enumeration<?> e = prop.propertyNames();
		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			String value = prop.getProperty(key);
			
			//Properties evaluated in the order they are in the file???
			m_variables.put(key,SubstituteVariable(value));
		}
	}
	
	private void LoadICPropertyFiles() throws Exception {
		Node n = m_pom.getPlugin("org.codehaus.mojo", "properties-maven-plugin");
		if (null==n) throw new Exception("ERROR - No Flyway Pluging in POM");
		
		NodeList nl = (NodeList) m_pom.getXPath().evaluate("x:configuration/x:files/x:file",n, XPathConstants.NODESET);
		for (int c=0;c<nl.getLength();c++) {
			LoadICPropertyFile(SubstituteVariable(nl.item(c).getTextContent()));
		};
	};
	
	//Load enviroment variables into m_variables
	private void LoadEnviroment() throws Exception {
		Map<String, String> env = System.getenv();
		for (String envName : env.keySet()) {
			m_variables.put("env." + envName,env.get(envName));
		}		
	};
	
	public void LoadVariables(String p_cmd_env, String p_actual_parent_sys) throws Exception {
		m_variables.put("ic.ci.resourcedir",GetResourceLocation());
		m_variables.put("ic.ci.flywaydir","TODO");
		m_variables.put("project.version",m_pom.getVersion());
		
		LoadEnviroment();
		
		m_variables.put("cmd.env",p_cmd_env);
		if (null==p_actual_parent_sys) throw new Exception("null actual parent sys");
		if (p_actual_parent_sys.equals("")) throw new Exception("blank actual parent sys");
		if (p_actual_parent_sys.equalsIgnoreCase("NULL")) throw new Exception("NULLV actual parent sys");
		m_variables.put("ic.actual.sys",p_actual_parent_sys);
		//Must be run last as this will rely on enviroment and command line params
		LoadICPropertyFiles();
		
		//DisplayVars();
		/*
		Variable Substitiotion Test Code
		String ttt;
		ttt = "ABC"; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		ttt = ""; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		ttt = "A${ic.ci.flywaydir}BC"; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		ttt = "A${ic.ci.flywaydir}B${ic.ci.resourcedir}C"; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		ttt = "${ic.db.jdbc.connectstring}"; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		m_variables.put("ic.ci.flywaydir.TODO","TODO");
		m_variables.put("TODO.ic.ci.flywaydir","TODO");
		m_variables.put("ic.ci.TODO.flywaydir","TODO");
		ttt = "${ic.ci.flywaydir.${ic.ci.flywaydir}}"; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		ttt = "AB${ic.ci.flywaydir.${ic.ci.flywaydir}}CD"; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		ttt = "AB${${ic.ci.flywaydir}.ic.ci.flywaydir}CD"; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		ttt = "AB${ic.ci.${ic.ci.flywaydir}.flywaydir}CD"; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		m_variables.put("TODO.TODO.TODO.TODO","TODO");
		ttt = "AB${${ic.ci.flywaydir}.${ic.ci.flywaydir}.${ic.ci.flywaydir}.${ic.ci.flywaydir}}CD"; System.out.println(ttt + "=" + SubstituteVariable(ttt));
		if (1==1) throw new Exception("TMP");
		*/		
		
		
	};

	protected abstract void SetLocalResourcePrefix(String p_Prefix) throws Exception;
	protected abstract void Download(String p_URLPrefix) throws Exception;
	protected abstract void Extract() throws Exception;
	public void Deploy() throws Exception {

		
		//DisplayVars();
		DeployINT();
	};
	
	//Can be overridden for non-default behavour
	public void DeployINT() throws Exception {
		List<PomFile.Execution> exes = m_pom.getExecutions();
		String workingDir = "";
		for (int c=0;c<exes.size();c++) {
			workingDir = SubstituteVariable(exes.get(c).getWorkingDirectory());
			File f = new File(workingDir);
			if (!f.exists()) {
				System.out.println(" - working directory for command not found - trying to create");
				if (!f.mkdirs()) {
					throw new Exception("Failed to create directory " + workingDir);
				};
			};
			ExecuteStep(m_pom, exes.get(c), workingDir);
		};
		
	};
	
	/*
	Can be overridden to give subclasses control of individual steps
	*/
	protected void ExecuteStep(PomFile p_pf, PomFile.Execution p_exe, String p_working_dir) throws Exception {
		List<String> args = new ArrayList<String>();
		for (int c=0;c<p_exe.getArguments().size();c++) {
			args.add(SubstituteVariable(p_exe.getArguments().get(c)));
		};
		Shell s = new Shell(
			SubstituteVariable(p_working_dir),
			SubstituteVariable(p_exe.getExecutable()),
			args,
			TimeUnit.MILLISECONDS.convert(60*10, TimeUnit.SECONDS)
		);
		s.execute();
		
	
	};
}