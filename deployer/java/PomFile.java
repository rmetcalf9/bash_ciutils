import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.namespace.NamespaceContext;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

/*
Class for reading a POM file
*/
public class PomFile {
	Document m_doc = null;
	XPath m_xPath = XPathFactory.newInstance().newXPath();
	private final String m_pomNS="http://maven.apache.org/POM/4.0.0";
	private String m_GroupID = "";
	private String m_ArtifactID = "";
	private String m_Version = "";
	private String m_sourceFileName = "";
	
	
	public class Dependancy {
		private String m_GroupID = "";
		private String m_ArtifactID = "";
		private String m_Version = "";
		private String m_Scope = "";
		public Dependancy(Node p_depNode) throws Exception {
			m_GroupID = m_xPath.evaluate("x:groupId",p_depNode);
			m_ArtifactID = m_xPath.evaluate("x:artifactId",p_depNode);
			m_Version = m_xPath.evaluate("x:version",p_depNode);
			m_Scope = m_xPath.evaluate("x:scope",p_depNode);
		};
		
		/*
		Used when we need to treat a pom as a dependancy
		*/
		public Dependancy(String p_GroupID, String p_ArtifactID, String p_Version) {
			m_GroupID=p_GroupID;
			m_ArtifactID=p_ArtifactID;
			m_Version=p_Version;
			m_Scope = ""; //Default to blank scope if we are creating this way
		};
		public String getGroupID() {return m_GroupID;};
		public String getArtifactID() {return m_ArtifactID;};
		public String getVersion() {return m_Version;};
		public String getScope() {return m_Scope;};
		public boolean isSnapshot() {return getVersion().contains("SNAPSHOT");}
		public String toString() {
			return getGroupID() + ":" + getArtifactID() + ":" + getVersion();
		};
		
	};
	
	/*
	helper method to allow us to refer to a pomfile as a dependancy
	*/
	public Dependancy getAsDependancy() {
		return new Dependancy(
			m_GroupID,
			m_ArtifactID,
			m_Version
		);
	};

	public PomFile(File p_input) throws Exception {
		m_sourceFileName = p_input.getAbsolutePath();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		m_doc = dBuilder.parse(p_input);
		
		m_xPath.setNamespaceContext(new NamespaceContext() {
			public String getNamespaceURI(String prefix) {
				if (prefix == null) throw new NullPointerException("Null prefix");
				else if ("x".equals(prefix)) return m_pomNS;
				//return XMLConstants.NULL_NS_URI;
				//return "m_pomNS";
				throw new NullPointerException("Unrecognised prefix " + prefix);
			}		
			// This method isn't necessary for XPath processing.
			public String getPrefix(String uri) {
				throw new UnsupportedOperationException();
			}

			// This method isn't necessary for XPath processing either.
			public Iterator getPrefixes(String uri) {
				throw new UnsupportedOperationException();
			}			
		});
		
		//<project xmlns="http://maven.apache.org/POM/4.0.0"
		if (!m_doc.getDocumentElement().getNodeName().equals("project")) throw new Exception("Not a POM file - no project root element (" + m_doc.getDocumentElement().getNodeName() + ")");
		if (!m_doc.getDocumentElement().getNamespaceURI().equals(m_pomNS)) throw new Exception("Not a POM file - wrong namespace for project root element (" + m_doc.getDocumentElement().getNamespaceURI() + ")");

		m_GroupID = m_xPath.evaluate("/x:project/x:groupId",m_doc.getDocumentElement());
		m_ArtifactID = m_xPath.evaluate("/x:project/x:artifactId",m_doc.getDocumentElement());
		m_Version = m_xPath.evaluate("/x:project/x:version",m_doc.getDocumentElement());
		
	}
	
	public String getSourceFileName() {
		return m_sourceFileName;
	};
	
	//XPath for extarnal classes to get xpath with x prefix built in
	public XPath getXPath() {return m_xPath;};
	
	public String getGroupID() {return m_GroupID;};
	public String getArtifactID() {return m_ArtifactID;};
	public String getVersion() {return m_Version;};
	public boolean isSnapshot() {return getVersion().contains("SNAPSHOT");}
	
	public String toString() {
		return getGroupID() + ":" + getArtifactID() + ":" + getVersion();
	};
	
	/****************************Dependancy******************************************/
	NodeList m_dependancies = null;
	private NodeList getDependancies() throws Exception {
		if (null!=m_dependancies) return m_dependancies;
		m_dependancies = (NodeList)m_xPath.evaluate(
				"/x:project/x:dependencies/x:dependency",
		        m_doc.getDocumentElement(), XPathConstants.NODESET
		);
		return m_dependancies;
	}
	
	public int getNumOfDependancies() throws Exception {
		return getDependancies().getLength();
	};
	
	public Dependancy getDependancy(int c) throws Exception {
		Node n = getDependancies().item(c);
		if (n==null) throw new Exception("Invalid Index");
		
		return new Dependancy(n);
	};
	/*******************************Dependancy END***************************************/
	
	private Map<String,String> m_properties = null;
	public Map<String,String> getProperties() throws Exception{
		if (null!=m_properties) return m_properties;
		m_properties = new HashMap<String,String>();

		NodeList nl = (NodeList)m_xPath.evaluate(
				"/x:project/x:properties/*",
		        m_doc.getDocumentElement(), XPathConstants.NODESET
		);
		
		for (int c=0;c<nl.getLength();c++) {
			String name = nl.item(c).getNodeName();
			String value = nl.item(c).getTextContent();
			m_properties.put(name,value);
		};
		
		return m_properties;
	};
	public String getProperty(String p_PropName) throws Exception {
		return getProperties().get(p_PropName);
	};
	/*
	Display properties for debugging
	*/
	public void displayProperties() throws Exception {
		for (Map.Entry<String, String> entry : getProperties().entrySet()) {
			String key = entry.getKey();
			System.out.println(key + "=" + entry.getValue());
		};
	};
	
	/****************************repositories******************************************/
	Map<String,String> m_repositories = null;
	private Map<String,String> getRepositories() throws Exception {
		if (null!=m_repositories) return m_repositories;
		NodeList nl = (NodeList)m_xPath.evaluate(
				"/x:project/x:repositories/x:repository",
		        m_doc.getDocumentElement(), XPathConstants.NODESET
		);
		m_repositories = new HashMap<String,String>();
		
		for (int c=0;c<nl.getLength();c++) {
			String id = m_xPath.evaluate("x:id",nl.item(c));
			String url = m_xPath.evaluate("x:url",nl.item(c));
			m_repositories.put(id,url);
		}
		
		return m_repositories;
	}
	public String getRepositoryURL(String p_id) throws Exception {
		return getRepositories().get(p_id);
	};
	/*******************************repositories END***************************************/
	/****************************executions******************************************/
	public class Execution {
		String m_id = "";
		String m_phase = "";
		String m_goal = "";
		String m_executable = "";
		List<String> m_arguments = new ArrayList<String>();
		String m_working_directory = "";
		public Execution(Node p_node) throws Exception {
			m_id = m_xPath.evaluate("x:id",p_node);
			m_phase = m_xPath.evaluate("x:phase",p_node);
			m_goal = m_xPath.evaluate("x:goals/x:goal",p_node);
			m_executable = m_xPath.evaluate("x:configuration/x:executable",p_node);
			m_working_directory = m_xPath.evaluate("x:configuration/x:workingDirectory",p_node);
			NodeList nl = (NodeList)m_xPath.evaluate(
				"x:configuration/x:arguments/x:argument",
		        p_node, XPathConstants.NODESET
			);
			for (int c=0;c<nl.getLength();c++) {
				m_arguments . add(nl.item(c).getTextContent());
			};
		};
		public String toString() {
			System.out.println("m_executable="+m_executable);
			System.out.println("m_working_directory="+m_working_directory);
			for (int c=0;c<m_arguments.size();c++) {
				System.out.println(" -a-" + m_arguments.get(c));
			};
			return "Exec:" + m_id + ":" + m_phase + ":" + m_goal;
		};
		public String getID() {return m_id;};
		public String getPhase() {return m_phase;};
		public String getGoal() {return m_goal;};
		public String getExecutable() {return m_executable;};
		public String getWorkingDirectory() {return m_working_directory;};
		public List<String> getArguments() {return m_arguments;};
	};
	
	private List<Execution> m_executions = null;
	public List<Execution> getExecutions() throws Exception {
		if (null!=m_executions) return m_executions;
		String xpath="/x:project/x:build/x:plugins/x:plugin[./x:groupId='org.codehaus.mojo' and ./x:artifactId='exec-maven-plugin']/x:executions/x:execution[./x:phase='compile']";		
		NodeList nl = (NodeList)m_xPath.evaluate(
				xpath,
		        m_doc.getDocumentElement(), XPathConstants.NODESET
		);
		
		List<Execution> ret = new ArrayList<Execution>();
		for (int c=0;c<nl.getLength();c++) {
			ret.add(new Execution(nl.item(c)));
		};
		return ret;
	};
	
	public Node getExecutionByID(String p_id) throws Exception {
		String xpath="/x:project/x:build/x:plugins/x:plugin/x:executions/x:execution[./x:id='" + p_id + "']";		
		return (Node)m_xPath.evaluate(
				xpath,
		        m_doc.getDocumentElement(), XPathConstants.NODE
		);
		
	};
	/*******************************executions END***************************************/
	
	public Node getPlugin(String p_groupID, String p_artifactID) throws Exception {
		String xpath = "/x:project/x:build/x:plugins/x:plugin[./x:groupId='" + p_groupID + "' and ./x:artifactId='" + p_artifactID + "']";
		return (Node)m_xPath.evaluate(
				xpath,
		        m_doc.getDocumentElement(), XPathConstants.NODE
		);		
	};
	
}