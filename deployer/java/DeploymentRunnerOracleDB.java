import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.namespace.NamespaceContext;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

import java.net.URL;

import org.flywaydb.core.Flyway;

/*
Class for running a Deployment file
*/
public class DeploymentRunnerOracleDB extends DeploymentRunner {
	private String m_recource_jar_file = "";

	public DeploymentRunnerOracleDB(PomFile p_pom, String p_tmpLocation) throws Exception  {
		super(p_pom, p_tmpLocation);
	};
	
	@Override
	protected void Download(String p_URLPrefix) throws Exception {
		String fn = p_URLPrefix + ".jar";
		m_recource_jar_file = m_tmpLocation + "/resource.jar";
		File f = new File(m_recource_jar_file);
		if (f.exists()) throw new Exception("File to download already exists (" + m_recource_jar_file + ")");
		//System.out.println(fn);
		//System.out.println(m_recource_jar_file);
		if (!DownloadUtils.downloadFromUrl(new URL(fn),m_recource_jar_file)) throw new Exception("Download failed 001");
		if (!f.exists()) throw new Exception("Download failed 002");
		
	};
	
	@Override
	protected void SetLocalResourcePrefix(String p_Prefix) throws Exception {
		m_recource_jar_file = p_Prefix + ".jar";
	}
	
	
	@Override
	protected void Extract() throws Exception {
		File f = new File(GetResourceLocation());
		f.mkdirs();
		DownloadUtils.ExtractJar(m_recource_jar_file, GetResourceLocation());
	};
	
	public static void ExecuteFlywayMigrate(
		String [] p_locations,
		String[] p_schemas,
		String p_url,
		String p_user,
		String p_password
	) throws Exception {
		System.out.println("**********Start Flyway Migrate**************");
		//System.out.println("url:" + url);
		Flyway flyway = new Flyway();
		flyway.setSchemas(p_schemas);
		flyway.setLocations(p_locations);
		flyway.setBaselineVersionAsString("0");
		flyway.setBaselineDescription("Initial");
		flyway.setBaselineOnMigrate(true);

		// Point it to the database
		flyway.setDataSource(p_url, p_user, p_password);

		// Start the migration
		flyway.migrate();
		System.out.println("**********End Flyway Migrate**************");
	};
	
	public static String RemoveSurroundingDoubleQuotesIfPresent(String p_str) {
		if (p_str.substring(0,1).equals("\"")) {
			if (p_str.substring(p_str.length()-1).equals("\"")) {
				//There may be more than one set of quotes
				return RemoveSurroundingDoubleQuotesIfPresent(p_str.substring(1,p_str.length()-1));
			};
		};
		return p_str;
	};

	@Override
	protected void ExecuteStep(PomFile p_pf, PomFile.Execution p_exe, String p_working_dir) throws Exception {
		if (p_exe.getID().equals("migrate-datastructures")) {
			String schema = "";
			String url = "";
			String user = "";
			String password = "";
			String location = "";
			/*
			Node flywayPluginXMLNode = p_pf.getPlugin("org.flywaydb", "flyway-maven-plugin");
			if (null==flywayPluginXMLNode) throw new Exception("ERROR - No Flyway Pluging in POM");
			
			url = SubstituteVariable(p_pf.getXPath().evaluate("x:configuration/x:url",flywayPluginXMLNode));
			user = SubstituteVariable(p_pf.getXPath().evaluate("x:configuration/x:user",flywayPluginXMLNode));
			password = SubstituteVariable(p_pf.getXPath().evaluate("x:configuration/x:password",flywayPluginXMLNode));
			
			NodeList nl = (NodeList)p_pf.getXPath().evaluate(
					"x:configuration/x:schemas/x:schema",
					flywayPluginXMLNode, XPathConstants.NODESET
			);
			schemas = new String[nl.getLength()];
			for (int c=0;c<nl.getLength();c++) {
				schemas[c] = SubstituteVariable(nl.item(c).getTextContent());
				//System.out.println(schemas[c]);
			};
			*/
			
			Node n = p_pf.getExecutionByID("migrate-datastructures");
			if (null==n) throw new Exception("Can't find execution with id migrate-datastructures");
			
			NodeList nl = (NodeList) p_pf.getXPath().evaluate("x:configuration/x:arguments/x:argument",n, XPathConstants.NODESET);
			for (int c=0;c<nl.getLength();c++) {
				String t = nl.item(c).getTextContent();

				t = RemoveSurroundingDoubleQuotesIfPresent(t);
				
				//Process any containing variables
				t = SubstituteVariable(t);
				
				switch (c) {
				case 0:
					url = t;
					break;
				case 1:
					user = t;
					break;
				case 2:
					password = t;
					break;
				case 3:
					schema = t;
					break;
				case 4:
					location = t;
					break;
				default:
					throw new Exception("Wrong number of arguments in migrate-datastructures");
				}
			};
			
			String[] schemas = new String[1];
			schemas[0] = schema;
			
			String[] locations = new String[1];
			locations[0] = "filesystem:" + location;
			
			//System.out.println("url=" + url);
			//System.out.println("user=" + user);
			//System.out.println("password=" + password);
			
			ExecuteFlywayMigrate(locations, schemas,url,user,password);
			
			
		} else {
			super.ExecuteStep(p_pf, p_exe, p_working_dir);
		};
	};
}