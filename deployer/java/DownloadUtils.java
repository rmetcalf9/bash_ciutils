import java.net.URL;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.net.URLConnection;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

class DownloadUtils {
	/*
	Logic to download a file from a repo
	*/
	public static boolean downloadPOMFromRepo(
		String p_repoURL, 
		String p_groupID, 
		String p_artifcatID,
		String p_version, 
		String p_localRoot
	) throws Exception {
		

		return true;
	};
	
	/*
	Gets metadata file from local REPO
	*/
	public static MavenMetaDataFile getLocalMetaDataFile(
		String p_localRepo, 
		String p_groupID, 
		String p_artifcatID,
		String p_version, 
		String p_localRoot
	) throws Exception {	
		//As it's local - no need to copy. Read it INLINE

		String metadata_xml_location = p_groupID.replace(".","/") + "/" + p_artifcatID.replace(".","/");
		String MetadataFileName = p_localRepo + "/" + metadata_xml_location + "/" + p_version + "/" + MavenMetaDataFile.localfilename;
		
		System.out.println(MetadataFileName);
		
		File f = new File(MetadataFileName);
		if (!f.exists()) {
			System.out.println(p_groupID + ":" + p_artifcatID + ":" + p_version + " not found in local repository");
			System.out.println("");
			return null;
		}
		
		MavenMetaDataFile meta = new MavenMetaDataFile(f, p_localRepo, true);
		if (!p_groupID.equals(meta.getGroupID())) throw new Exception("Internal Error - wront groupID");
		if (!p_artifcatID.equals(meta.getArtifactID())) throw new Exception("Internal Error - wront aartifcatID");
		if (!p_version.equals(meta.getVersion())) throw new Exception("Internal Error - wront version");
		
		return meta;

	}
	
	/*
	Gets metadata file from online REPO
	*/
	public static MavenMetaDataFile getMetaDataFile(
		String p_repoURL, 
		String p_groupID, 
		String p_artifcatID,
		String p_version, 
		String p_localRoot
	) throws Exception {	
		String metadata_xml_location = p_groupID.replace(".","/") + "/" + p_artifcatID.replace(".","/");
		String ArtifactURL = p_repoURL + "/" + metadata_xml_location + "/" + p_version;
		String ArtifaceLocal = p_localRoot + "/" + metadata_xml_location + "/" + p_version;

		File fff = new File(ArtifaceLocal);
		fff.mkdirs();
		
		//System.out.println("ArtifactURL=:" + ArtifactURL);
		//System.out.println("ArtifaceLocal=:" + ArtifaceLocal);
		
		File local_metadata_file = new File(ArtifaceLocal + "/" + MavenMetaDataFile.filename);
		
		if (!local_metadata_file.exists()) {
			if (!downloadFromUrl(new URL(ArtifactURL + "/" + MavenMetaDataFile.filename),local_metadata_file.getAbsolutePath())) return null;
			if (!local_metadata_file.exists()) throw new Exception("Download failed");
			
			MavenMetaDataFile meta = new MavenMetaDataFile(local_metadata_file, p_repoURL, false);
			if (!p_groupID.equals(meta.getGroupID())) throw new Exception("Internal Error - wront groupID");
			if (!p_artifcatID.equals(meta.getArtifactID())) throw new Exception("Internal Error - wront aartifcatID");
			if (!p_version.equals(meta.getVersion())) throw new Exception("Internal Error - wront version");
			
			return meta;
		};
		throw new Exception("Unexpected - metadata file already downloaded");
		
	}
	
	public static boolean downloadFromUrl(URL url, String localFilename) throws Exception {
		InputStream is = null;
		FileOutputStream fos = null;
		boolean success = false;
		fos = new FileOutputStream(localFilename, false);   //open outputstream to local file
		try {
			URLConnection urlConn = url.openConnection();//connect

			is = urlConn.getInputStream();               //get connection inputstream

			byte[] buffer = new byte[4096];              //declare 4KB buffer
			int len;

			//while we have availble data, continue downloading and storing to local file
			while ((len = is.read(buffer)) > 0) {  
				fos.write(buffer, 0, len);
			}
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("  download from " + url + " not found");
			success = false;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} finally {
				if (fos != null) {
					fos.close();
				}
			}
		}
		return success;
	}
	
    /*
     * Deletes a directory even if non-empty
     */    
    public static void DeleteDirectory(File p_dir) throws Exception {
      if (!p_dir.exists()) throw new Exception("ERROR Directory dosen't exist");
      File thisFile = null;
      List<File> m_Files = new ArrayList<File>();
      List<File> m_Dirs = new ArrayList<File>();
      for (int c=0;c<p_dir.listFiles().length;c++) {
          thisFile = p_dir.listFiles()[c];
          if (thisFile.isDirectory()) {
              m_Dirs.add(thisFile);
          } else {
              m_Files.add(thisFile);
          }
      };
      for (int c=0;c<m_Files.size();c++) {
          if (!m_Files.get(c).delete()) throw new Exception("ERROR Sub file deletion failed (" + m_Files.get(c).getName() + ")");
      }
      for (int c=0;c<m_Dirs.size();c++) {
          DeleteDirectory(m_Dirs.get(c));
      }
      p_dir.delete();
      
    }    
	
	/*
	* Code to extract jar file
	*/
	public static void ExtractJar(String p_jarFile, String p_destDir) throws Exception {
		java.util.jar.JarFile jar = new java.util.jar.JarFile(p_jarFile);
		java.util.Enumeration enumEntries = jar.entries();
		while (enumEntries.hasMoreElements()) {
			java.util.jar.JarEntry file = (java.util.jar.JarEntry) enumEntries.nextElement();
			java.io.File f = new java.io.File(p_destDir + java.io.File.separator + file.getName());
			if (file.isDirectory()) { // if its a directory, create it
				f.mkdir();
				continue;
			}
			java.io.InputStream is = jar.getInputStream(file); // get the input stream
			java.io.FileOutputStream fos = new java.io.FileOutputStream(f);
			while (is.available() > 0) {  // write contents of 'is' to 'fos'
				fos.write(is.read());
			}
			fos.close();
			is.close();
		}
		jar.close();
	}
	
}