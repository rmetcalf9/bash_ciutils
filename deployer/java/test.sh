cd ${CIUTILS_HOME}/deployer/java
rm *.class
javac -cp ${CIUTILS_HOME}/deployer/java:${CIUTILS_HOME}/deployer/java/jlib/flyway-core-4.0.3.jar Main.java Main.java
java -cp ${CIUTILS_HOME}/deployer/java:${CIUTILS_HOME}/deployer/java/jlib/flyway-core-4.0.3.jar:${CIUTILS_HOME}/deployer/java/jlib/ojdbc6-11.2.0.4.jar Main ~/deployable_artifacts/ICSchema_SAMPLE_SCHEMA/pom.xml ~/.m2/repository dev NULL
