#!/bin/bash

echo ""
echo "deployer.sh - Script to reploy artifacts and dependancies"

#The following line makes the script exit whenever an error occurs
set -e

##Compile java if required
if [ ! -f ${CIUTILS_HOME}/deployer/java/Main.class ]; then
	cd ${CIUTILS_HOME}/deployer/java
    javac -cp ${CIUTILS_HOME}/deployer/java:${CIUTILS_HOME}/deployer/java/jlib/flyway-core-4.0.3.jar Main.java
	if [ ! -f ${CIUTILS_HOME}/deployer/java/Main.class ]; then
        echo "ERROR Failed to find .class file"
        exit 1
	fi
fi

ARGS=""
for ((i=1; i<=$#; i++)); do
	ARGS="${ARGS} \"${!i}\""
done

java -cp ${CIUTILS_HOME}/deployer/java/jlib/flyway-core-4.0.3.jar:${CIUTILS_HOME}/deployer/java/jlib/ojdbc7.jar:${CIUTILS_HOME}/deployer/java Main ${ARGS}

echo ""
echo "end of deployer.sh"
echo ""
exit 0

