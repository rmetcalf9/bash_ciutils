
@echo "Start of ci_flyway batch file"

@cd %CIUTILS_HOME%\deployer\java

@IF EXIST %CIUTILS_HOME%\deployer\java\Ci_flyway.class goto skip_compile
javac -cp %CIUTILS_HOME%\deployer\java;%CIUTILS_HOME%\deployer\java\jlib\flyway-core-4.0.3.jar Ci_flyway.java
@IF ERRORLEVEL 1 GOTO compile_error
@IF NOT EXIST %CIUTILS_HOME%\deployer\java\Ci_flyway.class goto compile_error

:skip_compile


java -cp %CIUTILS_HOME%\deployer\java\jlib\flyway-core-4.0.3.jar;%CIUTILS_HOME%\deployer\java\jlib\ojdbc7.jar;%CIUTILS_HOME%\deployer\java Ci_flyway %1 %2 %3 %4 %5 %6 %7 %8 %9
@IF ERRORLEVEL 1 GOTO run_error

@echo "End of ci_flyway powershell script"
@goto end

:compile_error
@echo Compile Error
exit 1

:run_error
@echo Run Error
exit 1

:end
exit 0