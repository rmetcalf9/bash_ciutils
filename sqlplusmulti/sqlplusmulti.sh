#!/bin/bash

echo "**********************************************************************"
echo ""
echo "start of sqlplusmulti.sh - Script to run mutiple sql files"
echo ""

set -e

#Script will run sql plus mutiple times
# for every file in the folder that passes the filter
# script will exit on error

#The following line makes the script exit whenever an error occurs

CODEDIR=${1}
SQLPLUSCONNECTSTRING=${2}
SCHEMA=${3}
FILTER=${4}

if [ E${CODEDIR} == "E" ]; then
	echo "No params supplied"
	exit 1
fi

#Get-ChildItem "$sourcedir" -recurse -Filter  *.sql | 
#Foreach-Object {
#    $fn = $_.FullName
#    Write-Host "Executing $fn";
#    
#    $sql = "WHENEVER SQLERROR EXIT SQL.SQLCODE;`n"
#    $sql += "SET SERVEROUTPUT ON;`n" 
#    $sql += "ALTER SESSION SET CURRENT_SCHEMA=$schema;`n"
#    $sql += "@$fn;`n"
#    $sql += "/`n"
#    $sql += "exit;`n"
#
#    $sql | sqlplus $sqlplusconnectstring
#    if ( ! ( $? )) {
#	    echo "sqlobjectdeployer_getobjs: Error in SQL - couldn't get objects ($fn)"
#	    exit 1
#   }
#}

for f in $(find ${CODEDIR} -name ${FILTER}); do 
	echo $f; 
	sqlplus ${SQLPLUSCONNECTSTRING} <<EOF
	WHENEVER SQLERROR EXIT SQL.SQLCODE;
	SET SERVEROUTPUT ON;
	ALTER SESSION SET CURRENT_SCHEMA=${SCHEMA};
	@${f};
	/
	exit;
EOF
	SQLPLUS_RESULT=${?}
	if [ ${SQLPLUS_RESULT} -ne 0 ]; then
		echo "sqlplusmulti: Error in SQL - (${f})"
		exit 1
	fi	
done

echo ""
echo "end of sqlplusmulti.sh"
echo ""
exit 0
