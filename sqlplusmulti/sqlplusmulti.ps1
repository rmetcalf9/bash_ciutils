﻿Param(
  [string]$sourcedir,
  [string]$sqlplusconnectstring,
  [string]$schema,
  [string]$filter
)
$ErrorActionPreference = "Stop"

#Script will run sql plus mutiple times
# for every file in the folder that passes the filter
# script will exit on error

Write-Host "**********************************************************************"

Write-Host ""
Write-Host "Start of sqlplusmulti powershell script"

Get-ChildItem "$sourcedir" -recurse -Filter  $filter | 
Foreach-Object {
    $fn = $_.FullName
    Write-Host "Executing $fn";
    
    $sql = "WHENEVER SQLERROR EXIT SQL.SQLCODE;`n"
    $sql += "SET SERVEROUTPUT ON;`n" 
    $sql += "ALTER SESSION SET CURRENT_SCHEMA=$schema;`n"
    $sql += "@$fn;`n"
    $sql += "/`n"
    $sql += "exit;`n"

    $sql | sqlplus $sqlplusconnectstring
    if ( ! ( $? )) {
	    echo "sqlobjectdeployer_getobjs: Error in SQL - couldn't get objects ($fn)"
	    exit 1
    }
}

Write-Host "End of sqlplusmulti powershell script"
Write-Host ""

exit 0