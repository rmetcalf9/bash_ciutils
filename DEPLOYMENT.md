# Deploying schemas without the use of maven

It is possible to deploy schemas without the use of maven. This is only possible for the release versions of the artifacts.

An example command to achieve this is:
````
ci_deployer metcarob.com.oracledb.sample.schema ICSchema_SAMPLE_SCHEMA 0.0.3 https://mvn.metcarob.com rjmdev NULL
````

