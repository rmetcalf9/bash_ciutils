@echo _
@echo Script to install ciutilities
@echo _

@SET LBATDIR=C:\BATS

@IF "%CIUTILS_HOME%"=="" goto error_CIUTILS_HOME_not_defined
@IF NOT EXIST %LBATDIR% GOTO error_BAT_dir_missing

@IF EXIST %LBATDIR%\sqlcodedeploy.bat goto skip_001
@echo Writing sqlcodedeploy.bat
@echo @PowerShell.exe -Command "%CIUTILS_HOME%\sqlcodedeploy\sqlcodedeploy.ps1 %%1 %%2 %%3 %%4 %%5 %%6 %%7 %%8 %%9" > %LBATDIR%\sqlcodedeploy.bat
@echo @exit %%ERRORLEVEL%% >> %LBATDIR%\sqlcodedeploy.bat
:skip_001

@IF EXIST %LBATDIR%\sqlcodedeploy_setup.bat goto skip_002
@echo Writing sqlcodedeploy_setup.bat
@echo @PowerShell.exe -Command "%CIUTILS_HOME%\sqlcodedeploy\sqlcodedeploy_setup.ps1 %%1 %%2 %%3 %%4 %%5 %%6 %%7 %%8 %%9" > %LBATDIR%\sqlcodedeploy_setup.bat
@echo @exit %%ERRORLEVEL%% >> %LBATDIR%\sqlcodedeploy_setup.bat
:skip_002

@IF EXIST %LBATDIR%\sqlplusmulti.bat goto skip_003
@echo Writing sqlplusmulti.bat
@echo @PowerShell.exe -Command "%CIUTILS_HOME%\sqlplusmulti\sqlplusmulti.ps1 %%1 %%2 %%3 %%4 %%5 %%6 %%7 %%8 %%9" > %LBATDIR%\sqlplusmulti.bat
@echo @exit %%ERRORLEVEL%% >> %LBATDIR%\sqlplusmulti.bat
:skip_003

@IF EXIST %LBATDIR%\ci_deployer.bat goto skip_004
@echo Writing ci_deployer.bat
@echo @PowerShell.exe -Command "%CIUTILS_HOME%\deployer\ci_deployer.ps1 %%1 %%2 %%3 %%4 %%5 %%6 %%7 %%8 %%9" > %LBATDIR%\ci_deployer.bat
@echo @exit %%ERRORLEVEL%% >> %LBATDIR%\ci_deployer.bat
:skip_004

@IF EXIST %LBATDIR%\ci_flyway.bat goto skip_005
@echo Writing ci_flyway.bat
@echo @call %CIUTILS_HOME%\deployer\ci_flyway.bat > %LBATDIR%\ci_flyway.bat %%1 %%2 %%3 %%4 %%5 %%6 %%7 %%8 %%9
@echo @exit %%ERRORLEVEL%% >> %LBATDIR%\ci_flyway.bat
:skip_005


@IF EXIST %CIUTILS_HOME%\sqlcodedeploy\java\Main.class del %CIUTILS_HOME%\sqlcodedeploy\java\*.class
@IF EXIST %CIUTILS_HOME%\deployer\java\Main.class del %CIUTILS_HOME%\deployer\java\*.class


@echo _
@echo Sucessful end of script
@goto end

:error_CIUTILS_HOME_not_defined
@ECHO ERROR - CIUTILS_HOME is NOT defined
@echo  - it needs to point to the local directory for CIUTILS
@goto end

:error_BAT_dir_missing
@ECHO ERROR - Could not find %LBATDIR% directory
@echo  - it must be created and added to system path
@goto end


:end
@pause