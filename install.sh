#!/bin/bash

echo "Script to deploy CI utils on the CI server"

if [ ${#CIUTILS_HOME} -lt 1 ]; then
	echo "You must set the CIUTILS_HOME enviroment variable";
	exit 1
fi
if [ ! -d ${CIUTILS_HOME} ]; then
	echo "CIUTILS_HOME points to a non-existant directory ${CIUTILS_HOME}";
	exit 1
fi

HOMEDIR=$(echo ~)
MAINDIR=${CIUTILS_HOME}

function install {
	if [ ! -f ${1} ]; then
		echo "Error ${1} not found"
		exit 1
	fi
	TMP=$(basename ${1})
	INSTTO="${HOMEDIR}/bin/${TMP%.*}"

	if [ ! -d ${HOMEDIR}/bin ]; then
		mkdir -p ${HOMEDIR}/bin
	fi
	if [ ! -f ${INSTTO} ]; then
		ln -s ${1} ${INSTTO}
	fi

}

#Check we are running as SUDO
if [ "$EUID" -eq 0 ]; then
	echo "Please run as user"
	exit 1
else
	echo ""
fi

install "${MAINDIR}/sqlcodedeploy/sqlcodedeploy.sh"
install "${MAINDIR}/sqlcodedeploy/sqlcodedeploy_setup.sh"
install "${MAINDIR}/sqlplusmulti/sqlplusmulti.sh"
install "${MAINDIR}/deployer/ci_deployer.sh"
install "${MAINDIR}/deployer/ci_flyway.sh"

if [ -f ${CIUTILS_HOME}/sqlcodedeploy/java/Main.class ]; then
	rm ${CIUTILS_HOME}/sqlcodedeploy/java/*.class
fi
if [ -f ${CIUTILS_HOME}/deployer/java/Main.class ]; then
	rm ${CIUTILS_HOME}/deployer/java/*.class
fi

echo "Done"

exit 0

